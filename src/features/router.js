const express = require('express');
const utils = require('../utils');
const { ROLES } = require('../constants');
const authenticate = require('./account/middleware/authenticate');
const accessForRoles = require('./account/middleware/accessForRoles');

const {
  errorHandler,
  pageNotFoundHandler
} = utils.middleware;

const {
  isApiAvailable
} = utils;

const apiRouter = express.Router();
const router = express.Router();

router.get('/info', isApiAvailable);
router.use('/api/v1', apiRouter);

// -- start connecting routes
apiRouter.use('/accounts',
  require('./account/router'));

apiRouter.use('/properties',
  require('./property/router'));

apiRouter.use('/images',
  require('./image/router'));

apiRouter.use('/users',
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  require('./user/router'));

apiRouter.use('/reports',
  authenticate,
  require('./report/router'));

apiRouter.use('/files',
  authenticate,
  require('./reportFile/router'));

apiRouter.use('/agents',
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  require('./agent/router'));

apiRouter.use('/orders',
  authenticate,
  require('./order/router'));

apiRouter.use('/inspectors',
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  require('./inspector/router'));

// -- end connecting routes

router.use(pageNotFoundHandler);
router.use(errorHandler);

module.exports = router;
