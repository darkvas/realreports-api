const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const id = res.locals.params.id;

    const user = await authCognitoService.adminGetUser({ id });

    return responseSender.sendSuccess(res, user);
  } catch (ex) {
    return next(ex);
  }
};
