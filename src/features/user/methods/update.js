const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const id = res.locals.params.id;
    const {
      email,
      firstName,
      lastName
    } = res.locals.body;

    const user = await authCognitoService.adminUpdateUser({
      id,
      email,
      firstName,
      lastName
    });

    return responseSender.sendSuccess(res, user);
  } catch (ex) {
    return next(ex);
  }
};
