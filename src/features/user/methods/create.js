const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      email,
      password,
      firstName,
      phoneNumber,
      role
    } = res.locals.body;

    await authCognitoService.signUp({
      email,
      password,
      firstName,
      phoneNumber,
      role
    });
    await authCognitoService.adminConfirmSignUp({ email });
    const user = await authCognitoService.adminGetUser({ id: email }); // email could be used as id

    return responseSender.sendSuccess(res, user);
  } catch (ex) {
    return next(ex);
  }
};
