const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      limit,
      search
    } = res.locals.query;

    const paginationToken = res.locals.headers.paginationtoken;

    const {
      users          : items,
      paginationToken: nextPaginationToken
    } = await authCognitoService.getUserList({
      limit,
      paginationToken,
      search
    });

    return responseSender.sendSuccess(res, { items }, {
      currentPaginationToken: paginationToken,
      nextPaginationToken
    });
  } catch (ex) {
    return next(ex);
  }
};
