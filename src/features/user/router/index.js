const express = require('express');
const utils = require('../../../utils');
const schemas = require('../user.schema');

const {
  middleware: {
    validateRequest
  }
} = utils;

const router = express.Router();

router.get('/',
  validateRequest(schemas.paginationToken, 'headers'),
  validateRequest(schemas.getList, 'query'),
  require('../methods/getList'));

router.post('/',
  validateRequest(schemas.create, 'body'),
  require('../methods/create'));

router.get('/:id',
  validateRequest(schemas.id, 'params'),
  require('../methods/getOne'));

router.put('/:id',
  validateRequest(schemas.id, 'params'),
  validateRequest(schemas.update, 'body'),
  require('../methods/update'));

router.delete('/:id',
  validateRequest(schemas.id, 'params'),
  require('../methods/delete'));

module.exports = router;
