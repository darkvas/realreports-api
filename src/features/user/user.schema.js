const joi = require('joi');
const phoneJoi = joi.extend(require('joi-phone-number'));
const CONSTANTS = require('../../constants');

const {
  REGEX_PASS,
  REGEX_PASS_OPTIONS,
  PASS_CUSTOM_MESSAGE
} = CONSTANTS.VALIDATIONS;
const ROLES = CONSTANTS.ROLES;

const getList = joi.object().keys({
  limit : joi.number().integer().min(1).max(100).default(10),
  search: joi.object().keys({
    field: joi.string().valid([
      'id', 'email', 'firstName', 'lastName', 'phoneNumber'
    ]).required(),
    value: joi.string().min(1).max(50).required()
  }).default(null)
});

const paginationToken = joi.object().keys({
  paginationtoken: joi.string().max(2000).default(null)
});

const id = joi.object().keys({
  id: joi.string().max(2000).required()
});

const update = joi.object().keys({
  email    : joi.string().email().required(),
  firstName: joi.string().min(1).max(100).required(),
  lastName : joi.string().min(1).max(100).required()
});

const create = phoneJoi.object().keys({
  email      : phoneJoi.string().email().required(),
  password   : phoneJoi.string().min(8).max(32).regex(REGEX_PASS, REGEX_PASS_OPTIONS).required().error(() => PASS_CUSTOM_MESSAGE),
  firstName  : phoneJoi.string().min(1).max(100).required(),
  phoneNumber: phoneJoi.string().phoneNumber({ defaultCountry: 'AU', format: 'e164' }).required(),
  role       : phoneJoi.string().valid([ROLES.BUYER, ROLES.ADMIN]).required()
});

module.exports = {
  getList,
  paginationToken,
  id,
  update,
  create
};
