const express = require('express');
const utils = require('../../../utils');
const schemas = require('../property.schema');
const authenticate = require('../../account/middleware/authenticate');
const accessForRoles = require('../../account/middleware/accessForRoles');
const { ROLES } = require('../../../constants');

const {
  middleware: {
    validateRequest
  }
} = utils;

const router = express.Router();

router.post('/',
  validateRequest(schemas.create, 'body'),
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  require('../methods/create'));

router.get('/',
  validateRequest(schemas.getList, 'query'),
  require('../methods/getList'));

router.get('/search',
  validateRequest(schemas.search, 'query'),
  require('../methods/search'));

router.get('/count',
  validateRequest(schemas.search, 'query'),
  require('../methods/count'));

router.post('/requestProperty',
  validateRequest(schemas.requestProperty, 'body'),
  require('../methods/requestProperty'));

router.post('/:id/requestReport',
  validateRequest(schemas.id, 'params'),
  validateRequest(schemas.requestReport, 'body'),
  require('../methods/requestReport'));

router.put('/:id',
  validateRequest(schemas.id, 'params'),
  validateRequest(schemas.update, 'body'),
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  require('../methods/update'));

router.delete('/:id',
  validateRequest(schemas.id, 'params'),
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  require('../methods/delete'));

router.get('/:id',
  validateRequest(schemas.id, 'params'),
  require('../methods/getOne'));

module.exports = router;
