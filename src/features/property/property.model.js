const _ = require('lodash');
const database = require('../../services/database');
const sqlManager = require('../../services/sqlManager');

module.exports = {
  create: async options => {
    const {
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      addressFull,
      title,
      description,
      score,
      createdBy
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'create');
    const result = await database.query(sqlQuery, [
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      addressFull,
      title,
      description,
      score,
      createdBy
    ]);

    return _.get(result, 'rows[0]', {});
  },

  update: async options => {
    const {
      id,
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      addressFull,
      title,
      description,
      score
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'update');
    const result = await database.query(sqlQuery, [
      id,
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      addressFull,
      title,
      description,
      score
    ]);

    return _.get(result, 'rows[0]', {});
  },

  remove: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'delete');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  getOne: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getOne');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  count: async ({ search }) => {
    const searchString = `%${search.toLowerCase()}%`;

    const sqlQuery = sqlManager.getSql(__dirname, 'count');
    const result = await database.query(sqlQuery, [searchString]);

    return _.get(result, 'rows[0].count', 0);
  },

  getList: async options => {
    const {
      limit,
      offset,
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      zipcodeFrom,
      zipcodeTo,
      excludeIds
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getList');
    const result = await database.query(sqlQuery, [
      limit,
      offset,
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      zipcodeFrom,
      zipcodeTo,
      excludeIds
    ]);

    return result.rows;
  },

  search: async options => {
    const {
      limit,
      offset,
      search
    } = options;

    const searchString = `%${search.toLowerCase()}%`;

    const sqlQuery = sqlManager.getSql(__dirname, 'search');
    const result = await database.query(sqlQuery, [
      limit,
      offset,
      searchString
    ]);

    return result.rows;
  }
};
