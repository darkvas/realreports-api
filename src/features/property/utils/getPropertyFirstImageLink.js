const mapImages = require('./mapImages');

module.exports = property => {
  if (property.images && property.images.length) {
    if (!property.images[0].href) {
      property.images = mapImages(property.images);
    }

    return property.images[0].href;
  }

  return '';
};
