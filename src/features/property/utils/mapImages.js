const generateImageLink = require('../../image/utils/generateImageLink');

module.exports = images => {
  return images
    .filter(item => item && item.id)
    .map(item => ({
      id  : item.id,
      name: item.name,
      href: generateImageLink(item)
    }));
};
