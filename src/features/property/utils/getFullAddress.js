// todo: check with google places format
module.exports = propertyData => {
  const {
    // country,
    state,
    // zipcode,
    city,
    street,
    streetNumber
  } = propertyData;

  return `${streetNumber} ${street}, ${city}, ${state}`;
};
