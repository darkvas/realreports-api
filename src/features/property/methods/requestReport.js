const _ = require('lodash');
const propertyModel = require('../property.model');
const emailHandler = require('../../email/email.handler');
const config = require('../../../../config');
const {
  responseSender,
  customError: {
    BadRequestError
  }
} = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      email,
      firstName,
      lastName,
      phoneNumber
    } = res.locals.body;

    const propertyId = res.locals.params.id;

    const property = await propertyModel.getOne({ id: propertyId });
    if (!property || !property.id) {
      return next(new BadRequestError(`Not found property with id: ${propertyId}`));
    }

    const agent = _.get(property, 'agents[0]', {});
    /* if (!agent) {
      return next(new BadRequestError(`Not found agent for property with id: ${propertyId}`));
    } */

    const cheatAgent1 = { email: config.emailToCheat1 };
    const cheatAgent2 = { email: config.emailToCheat2 };

    const user = {
      email,
      firstName,
      lastName,
      phoneNumber
    };

    await emailHandler.notifyRequestReportToUser(property, agent, user);

    await emailHandler.notifyRequestReport(property, cheatAgent1, user);

    const result = await emailHandler.notifyRequestReport(property, cheatAgent2, user);

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
