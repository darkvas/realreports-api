const propertyModel = require('../property.model');
const utils = require('../../../utils');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      search
    } = res.locals.query;

    const count = await propertyModel.count({ search });

    return responseSender.sendSuccess(res, { count });
  } catch (ex) {
    return next(ex);
  }
};
