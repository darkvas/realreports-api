const propertyModel = require('../property.model');
const utils = require('../../../utils');
const mapImages = require('../utils/mapImages');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      limit,
      offset,
      search
    } = res.locals.query;

    const items = await propertyModel.search({
      limit,
      offset,
      search
    });

    items.forEach(property => {
      if (property.images && property.images.length) {
        property.images = mapImages(property.images);
      }
      if (property.agents && property.agents.length) {
        property.agents = property.agents.filter(agent => agent.id);
      }
    });

    return responseSender.sendSuccess(res, { items });
  } catch (ex) {
    return next(ex);
  }
};
