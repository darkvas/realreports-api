const propertyModel = require('../property.model');
const getFullAddress = require('../utils/getFullAddress');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      title,
      description,
      score
    } = res.locals.body;

    const id = res.locals.params.id;

    const addressFull = getFullAddress({
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber
    });

    const result = await propertyModel.update({
      id,
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber,
      addressFull,
      title,
      description,
      score
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
