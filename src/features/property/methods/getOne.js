const propertyModel = require('../property.model');
const agentModel = require('../../agent/agent.model');
const utils = require('../../../utils');
const mapImages = require('../utils/mapImages');
const mapImagesForAgent = require('../../agent/utils/mapImagesForAgent');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const id = res.locals.params.id;

    const property = await propertyModel.getOne({ id });

    if (property && property.id) {
      const agents = await agentModel.getList({
        offset    : 0,
        limit     : 1000,
        propertyId: property.id
      });
      agents.forEach(mapImagesForAgent);
      property.agents = agents;

      if (property.images && property.images.length) {
        property.images = mapImages(property.images);
      }
    }

    return responseSender.sendSuccess(res, property);
  } catch (ex) {
    return next(ex);
  }
};
