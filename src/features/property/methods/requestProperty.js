const emailHandler = require('../../email/email.handler');
const config = require('../../../../config');
const {
  responseSender
} = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      email,
      firstName,
      lastName,
      phoneNumber,
      searchAddress,
      agentName,
      agentContact,
      agencyTitle
    } = res.locals.body;

    const cheatAgent1 = { email: config.emailToCheat1 };
    const cheatAgent2 = { email: config.emailToCheat2 };

    const propertyInfo = {
      searchAddress,
      agentName,
      agentContact,
      agencyTitle
    };

    const userInfo = {
      email,
      firstName,
      lastName,
      phoneNumber
    };

    await emailHandler.notifyRequestProperty(propertyInfo, cheatAgent1, userInfo);

    const result = await emailHandler.notifyRequestProperty(propertyInfo, cheatAgent2, userInfo);

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
