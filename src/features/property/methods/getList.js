const propertyModel = require('../property.model');
const utils = require('../../../utils');
const mapImages = require('../utils/mapImages');

const responseSender = utils.responseSender;

const PROPOSAL_ZIPCODE_RANGE = 100; // + or - this range
const PROPOSAL_LIMIT = 10;
const PROPOSAL_OFFSET = 0;

const getSearchParams = (searchObject = {}) => {
  const defaultSearchParams = {
    country     : null,
    state       : null,
    zipcode     : null,
    city        : null,
    street      : null,
    streetNumber: null
  };

  Object.keys(searchObject).forEach(key => {
    const val = searchObject[key];
    if (val) {
      if (key === 'zipcode') {
        searchObject[key] = val;
      } else {
        searchObject[key] = `%${val}%`;
      }
    }
  });

  return Object.assign(defaultSearchParams, searchObject);
};

const getProposalItems = async (searchParams, excludeIds) => {
  const {
    country,
    state,
    zipcode,
    city
  } = searchParams;

  let zipcodeFrom = zipcode - PROPOSAL_ZIPCODE_RANGE;
  if (zipcodeFrom < 1) {
    zipcodeFrom = 1;
  }

  const zipcodeTo = zipcode + PROPOSAL_ZIPCODE_RANGE;

  const items = await propertyModel.getList({
    limit       : PROPOSAL_LIMIT,
    offset      : PROPOSAL_OFFSET,
    country,
    state,
    zipcode     : null, // do not search zipcode, street, streetNumber
    city,
    street      : null,
    streetNumber: null,
    zipcodeFrom,
    zipcodeTo,
    excludeIds
  });

  items.forEach(property => {
    if (property.images && property.images.length) {
      property.images = mapImages(property.images);
    }
  });

  return items;
};

module.exports = async (req, res, next) => {
  try {
    const {
      limit,
      offset,
      s
    } = res.locals.query;

    const searchParams = getSearchParams(s);

    const {
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber
    } = searchParams;

    const items = await propertyModel.getList({
      limit,
      offset,
      country,
      state,
      zipcode,
      city,
      street,
      streetNumber
    });

    items.forEach(property => {
      if (property.images && property.images.length) {
        property.images = mapImages(property.images);
      }
    });

    let proposalItems = [];
    // in case if search has zipcode add proposal items
    if (s && s.zipcode) {
      const excludeIds = items.map(property => property.id);
      proposalItems = await getProposalItems(searchParams, excludeIds);
    }

    return responseSender.sendSuccess(res, { items, proposalItems });
  } catch (ex) {
    return next(ex);
  }
};
