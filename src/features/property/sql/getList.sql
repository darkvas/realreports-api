SELECT
  properties.id,
  properties."country",
  properties."state",
  properties."zipcode",
  properties."city",
  properties."street",
  properties."streetNumber",
  properties."title",
  properties."description",
  properties."score",
  properties."createdBy",
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', images.id,
      'name', images."name",
      'link', images."link",
      'referenceId', images."referenceId",
      'referenceTable', images."referenceTable"
    ))
    FROM images
      WHERE images."referenceTable" = 'properties'
      AND images."referenceId" = properties.id
    GROUP BY images."referenceId"
  ), '[]') as images,
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', agents.id,
      'firstName', agents."firstName",
      'lastName', agents."lastName",
      'email', agents."email",
      'phoneNumber', agents."phoneNumber",
      'agencyTitle', agents."agencyTitle"
    ))
    FROM agents
    INNER JOIN agents_properties ON agents_properties."agentId" = agents.id
      WHERE agents_properties."propertyId" = properties.id
      AND agents."deletedAt" IS NULL
    GROUP BY agents_properties."propertyId"
  ), '[]') as agents,
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', reports.id,
      'title', reports."title",
      'description', reports."description",
      'score', reports."score",
      'price', reports."price",
      'currency', reports."currency",
      'date', reports."date"
    ))
    FROM reports
      WHERE reports."propertyId" = properties.id
      AND reports."deletedAt" IS NULL
    GROUP BY reports."propertyId"
  ), '[]') as reports
FROM properties
WHERE properties."deletedAt" IS NULL
  AND ($3::text IS NULL OR "country" ILIKE $3)
  AND ($4::text IS NULL OR "state" ILIKE $4)
  AND ($5::int IS NULL OR CAST("zipcode" AS INTEGER) = $5)
  AND ($6::text IS NULL OR "city" ILIKE $6)
  AND ($7::text IS NULL OR "street" ILIKE $7)
  AND ($8::text IS NULL OR "streetNumber" ILIKE $8)
  AND ($9::int IS NULL OR CAST("zipcode" AS INTEGER) BETWEEN $9 AND $10)
  AND ($11::int[] IS NULL OR (NOT (properties.id = ANY ($11::int[]))))
ORDER BY properties.id DESC
  LIMIT $1 OFFSET $2;
