UPDATE properties
  SET "country" = $2,
      "state" = $3,
      "zipcode" = $4,
      "city" = $5,
      "street" = $6,
      "streetNumber" = $7,
      "addressFull" = $8,
      "title" = $9,
      "description" = $10,
      "score" = $11,
      "updatedAt" = NOW()
  WHERE id = $1
    AND "deletedAt" is null
  RETURNING "id",
            "country",
            "state",
            "zipcode",
            "city",
            "street",
            "streetNumber",
            "addressFull",
            "title",
            "description",
            "score",
            "updatedAt";
