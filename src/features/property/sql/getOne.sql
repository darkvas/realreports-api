SELECT
  properties.id,
  properties."country",
  properties."state",
  properties."zipcode",
  properties."city",
  properties."street",
  properties."streetNumber",
  properties."title",
  properties."description",
  properties."score",
  properties."createdBy",
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', images.id,
      'name', images."name",
      'link', images."link",
      'referenceId', images."referenceId",
      'referenceTable', images."referenceTable"
    ))
    FROM images
      WHERE images."referenceTable" = 'properties'
      AND images."referenceId" = properties.id
    GROUP BY images."referenceId"
  ), '[]') as images,
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', agents.id,
      'firstName', agents."firstName",
      'lastName', agents."lastName",
      'email', agents."email",
      'phoneNumber', agents."phoneNumber",
      'agencyTitle', agents."agencyTitle"
    ))
    FROM agents
    INNER JOIN agents_properties ON agents_properties."agentId" = agents.id
      WHERE agents_properties."propertyId" = properties.id
      AND agents."deletedAt" IS NULL
    GROUP BY agents_properties."propertyId"
  ), '[]') as agents,
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', inspectors.id,
      'options', inspectors."options"
    ))
    FROM inspectors
    INNER JOIN inspectors_properties ON inspectors_properties."inspectorId" = inspectors.id
      WHERE inspectors_properties."propertyId" = properties.id
      AND inspectors."deletedAt" IS NULL
    GROUP BY inspectors_properties."propertyId"
  ), '[]') as inspectors,
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', reports.id,
      'title', reports."title",
      'description', reports."description",
      'score', reports."score",
      'price', reports."price",
      'currency', reports."currency",
      'date', reports."date",
      'options', reports."options"
    ))
    FROM reports
      WHERE reports."propertyId" = properties.id
      AND reports."deletedAt" IS NULL
    GROUP BY reports."propertyId"
  ), '[]') as reports
FROM properties
WHERE properties.id = $1
  AND "deletedAt" IS NULL
