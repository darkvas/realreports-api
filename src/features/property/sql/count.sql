SELECT
  count(id)
FROM "properties"
WHERE properties."deletedAt" IS NULL
  AND ($1 = '%%' OR ("addressFull" ILIKE $1
      OR "country" ILIKE $1
      OR "zipcode" ILIKE $1
      OR "title" ILIKE $1
      OR "description" ILIKE $1))
