SELECT
  properties.id,
  properties."country",
  properties."state",
  properties."zipcode",
  properties."city",
  properties."street",
  properties."streetNumber",
  properties."title",
  properties."description",
  properties."score",
  properties."createdBy",
  json_agg(json_build_object(
    'id', images.id,
    'name', images."name",
    'link', images."link",
    'referenceId', images."referenceId",
    'referenceTable', images."referenceTable"
  )) AS images,
  COALESCE((SELECT
    json_agg(json_build_object(
      'id', agents.id,
      'firstName', agents."firstName",
      'lastName', agents."lastName",
      'email', agents."email",
      'phoneNumber', agents."phoneNumber",
      'agencyTitle', agents."agencyTitle"
    ))
    FROM agents
    INNER JOIN agents_properties ON agents_properties."agentId" = agents.id
      WHERE agents_properties."propertyId" = properties.id
      AND agents."deletedAt" IS NULL
    GROUP BY agents_properties."propertyId"
  ), '[]') as agents
FROM properties
  LEFT JOIN images ON images."referenceId" = properties.id AND images."referenceTable" = 'properties'
WHERE properties."deletedAt" IS NULL
  AND ($3 = '%%' OR ("addressFull" ILIKE $3
      OR "country" ILIKE $3
      OR "zipcode" ILIKE $3
      OR "title" ILIKE $3
      OR "description" ILIKE $3))
GROUP BY
  properties.id,
  properties."country",
  properties."state",
  properties."zipcode",
  properties."city",
  properties."street",
  properties."streetNumber",
  properties."title",
  properties."description",
  properties."score",
  properties."createdBy"
ORDER BY properties.id desc
  LIMIT $1 OFFSET $2;

