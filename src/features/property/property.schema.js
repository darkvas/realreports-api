const joi = require('joi');
const phoneJoi = joi.extend(require('joi-phone-number'));
const { DEFAULT_VALUES } = require('../../constants');

const create = joi.object().keys({
  country     : joi.string().max(50).default('Australia'),
  state       : joi.string().max(50).required(),
  zipcode     : joi.number().integer().min(1).max(999999).required(),
  city        : joi.string().max(50).required(),
  street      : joi.string().max(50).required(),
  streetNumber: joi.string().max(50).required(),
  title       : joi.string().max(100).allow(''),
  description : joi.string().max(3000).allow(''),
  score       : joi.number().precision(2).min(0).max(10).allow(null)
});

const update = joi.object().keys({
  country     : joi.string().max(50).default('Australia'),
  state       : joi.string().max(50).required(),
  zipcode     : joi.number().integer().min(1).max(999999).required(),
  city        : joi.string().max(50).required(),
  street      : joi.string().max(50).required(),
  streetNumber: joi.string().max(50).required(),
  title       : joi.string().max(100).allow(''),
  description : joi.string().max(3000).allow(''),
  score       : joi.number().precision(2).min(0).max(10).allow(null)
});

const getList = joi.object().keys({
  limit : joi.number().integer().min(1).max(100).default(DEFAULT_VALUES.LIMIT),
  offset: joi.number().integer().min(0).default(DEFAULT_VALUES.OFFSET),
  s     : joi.object().keys({
    country     : joi.string().min(1).max(50),
    state       : joi.string().min(1).max(50),
    zipcode     : joi.number().integer().min(1).max(999999),
    city        : joi.string().min(1).max(50),
    street      : joi.string().min(1).max(50),
    streetNumber: joi.string().min(1).max(50)
  })
});

const search = joi.object().keys({
  limit : joi.number().integer().min(1).max(100).default(DEFAULT_VALUES.LIMIT),
  offset: joi.number().integer().min(0).default(DEFAULT_VALUES.OFFSET),
  search: joi.string().max(50).default('')
});

const id = joi.object().keys({
  id: joi.number().integer().min(1).required()
});

const requestReport = phoneJoi.object().keys({
  firstName  : phoneJoi.string().min(1).max(100).required(),
  lastName   : phoneJoi.string().min(1).max(100).required(),
  phoneNumber: phoneJoi.string().phoneNumber({ defaultCountry: DEFAULT_VALUES.PHONE_COUNTRY, format: DEFAULT_VALUES.PHONE_FORMAT }).required(),
  email      : phoneJoi.string().email().required()
});

const requestProperty = requestReport.append({
  searchAddress: phoneJoi.string().min(1).max(500).required(),
  agentName    : phoneJoi.string().max(100).allow(''),
  agentContact : phoneJoi.string().max(100).allow(''),
  agencyTitle  : phoneJoi.string().max(100).allow('')
});

module.exports = {
  create,
  update,
  getList,
  search,
  id,
  requestReport,
  requestProperty
};
