SELECT
  reports.id,
  reports."author",
  reports."createdAt",
  reports."createdBy",
  reports."currency",
  reports."description",
  reports."price",
  reports."propertyId",
  reports."score",
  reports."title",
  reports."date",
  COALESCE(reports."options", '{}') as "options"
FROM reports
  WHERE reports."deletedAt" IS NULL
  AND reports."propertyId" = $1
  ORDER BY reports."id" DESC
