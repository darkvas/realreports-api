SELECT
  reports.id,
  reports."author",
  reports."createdAt",
  reports."createdBy",
  reports."currency",
  reports."description",
  reports."price",
  reports."propertyId",
  reports."score",
  reports."title",
  reports."date",
  COALESCE(reports."options", '{}') as "options",
  json_build_object(
    'id', report_files.id,
    'name', report_files."name",
    'link', report_files."link",
    'reportId', report_files."reportId",
    'contentType', report_files."contentType"
  ) as "reportFile"
FROM reports
LEFT JOIN report_files ON report_files."id" = (
    SELECT "id" FROM report_files
    WHERE report_files."reportId" = reports."id"
    ORDER BY report_files."id" DESC
    LIMIT 1 )
WHERE reports."id" = $1
  AND reports."deletedAt" IS NULL

