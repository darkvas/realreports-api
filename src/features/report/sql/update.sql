UPDATE reports
  SET "author" = $2,
      "currency" = $3,
      "description" = $4,
      "price" = $5,
      "propertyId" = $6,
      "score" = $7,
      "title" = $8,
      "date" = $9,
      "options" = $10,
      "updatedAt" = NOW()
  WHERE id = $1
    AND "deletedAt" is null
  RETURNING "id",
            "author",
            "createdAt",
            "createdBy",
            "currency",
            "description",
            "price",
            "propertyId",
            "score",
            "title",
            "date",
            "options";
