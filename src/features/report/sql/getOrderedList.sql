SELECT
  reports.id,
  reports."author",
  reports."createdAt",
  reports."createdBy",
  reports."currency",
  reports."description",
  reports."price",
  reports."score",
  reports."title",
  reports."date",
  COALESCE(reports."options", '{}') as "options",
  json_build_object(
    'id', report_files.id,
    'name', report_files."name",
    'link', report_files."link",
    'reportId', report_files."reportId"
  ) AS "reportFile",
  json_build_object(
    'id', properties.id,
    'country', properties."country",
    'state', properties."state",
    'zipcode', properties."zipcode",
    'city', properties."city",
    'street', properties."street",
    'streetNumber', properties."streetNumber",
    'title', properties."title",
    'description', properties."description"
  ) AS property
FROM reports
  INNER JOIN orders ON orders."reportId" = reports.id AND orders."userId" = $3
  INNER JOIN properties ON properties.id = reports."propertyId"
  LEFT JOIN report_files ON report_files."id" = (
    SELECT "id" FROM report_files
    WHERE report_files."reportId" = reports."id"
    ORDER BY report_files."id" DESC
    LIMIT 1 )
  WHERE reports."deletedAt" IS NULL
  ORDER BY reports."id" DESC
    LIMIT $1 OFFSET $2;
