INSERT INTO reports
  ( "author",
    "createdBy",
    "currency",
    "description",
    "price",
    "propertyId",
    "score",
    "title",
    "date",
    "options")
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
    RETURNING "id",
              "author",
              "currency",
              "description",
              "price",
              "propertyId",
              "score",
              "title",
              "date",
              "options",
              "createdBy";
