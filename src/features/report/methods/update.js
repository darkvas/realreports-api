const reportModel = require('../report.model');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      author,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    } = res.locals.body;

    const id = res.locals.params.id;

    const result = await reportModel.update({
      id,
      author,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
