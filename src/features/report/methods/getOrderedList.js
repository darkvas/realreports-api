const reportModel = require('../report.model');
const utils = require('../../../utils');
const generateFileLink = require('../../reportFile/utils/generateFileLink');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      limit,
      offset
    } = res.locals.query;

    const userId = res.locals.user.id;

    const items = await reportModel.getOrderedList({
      userId,
      limit,
      offset
    });

    items.forEach(report => {
      if (report.reportFile && report.reportFile.id) {
        report.reportFile = {
          id  : report.reportFile.id,
          name: report.reportFile.name,
          href: generateFileLink(report.reportFile)
        };
      } else {
        report.reportFile = null;
      }
    });

    return responseSender.sendSuccess(res, { items });
  } catch (ex) {
    return next(ex);
  }
};
