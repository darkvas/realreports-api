const _ = require('lodash');
const reportModel = require('../report.model');
const {
  responseSender,
  customError: {
    BadRequestError
  }
} = require('../../../utils');

const ALLOW_REPORTS_PER_PROPERTY = 1;

module.exports = async (req, res, next) => {
  try {
    const {
      author,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    } = res.locals.body;

    const createdBy = _.get(res, 'locals.user.id', null);

    const propertyReports = await reportModel.getListForProperty({ propertyId });
    if (propertyReports && propertyReports.length && propertyReports.length >= ALLOW_REPORTS_PER_PROPERTY) {
      return next(new BadRequestError(`Property with id: ${propertyId} already has ${ALLOW_REPORTS_PER_PROPERTY} report`));
    }

    const result = await reportModel.create({
      author,
      createdBy,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
