const reportModel = require('../report.model');
const utils = require('../../../utils');
const generateFileLink = require('../../reportFile/utils/generateFileLink');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const id = res.locals.params.id;

    const report = await reportModel.getOne({ id });

    if (report && report.id) {
      report.reportFile = report.reportFile && report.reportFile.id ? {
        id  : report.reportFile.id,
        name: report.reportFile.name,
        href: generateFileLink(report.reportFile)
      } : null;
    }

    return responseSender.sendSuccess(res, report);
  } catch (ex) {
    return next(ex);
  }
};
