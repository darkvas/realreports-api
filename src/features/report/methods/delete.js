const reportModel = require('../report.model');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const id = res.locals.params.id;

    const result = await reportModel.remove({ id });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
