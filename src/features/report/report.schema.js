const joi = require('joi');

const create = joi.object().keys({
  propertyId : joi.number().integer().min(1).required(),
  author     : joi.string().max(50).allow(''),
  title      : joi.string().max(100).allow(''),
  description: joi.string().max(300).allow(''),
  score      : joi.number().precision(2).min(0).max(10).allow(null),
  price      : joi.number().precision(2).min(0).required(),
  currency   : joi.string().min(1).max(50).default('AUD'),
  date       : joi.date().iso().required(),
  options    : joi.array().items(joi.string().required()).min(1).required()
});

const update = joi.object().keys({
  propertyId : joi.number().integer().min(1).required(),
  author     : joi.string().max(50).allow(''),
  title      : joi.string().max(100).allow(''),
  description: joi.string().max(300).allow(''),
  score      : joi.number().precision(2).min(0).max(10).allow(null),
  price      : joi.number().precision(2).min(0).required(),
  currency   : joi.string().min(1).max(50).default('AUD'),
  date       : joi.date().iso().required(),
  options    : joi.array().items(joi.string().required()).min(1).required()
});

const getList = joi.object().keys({
  limit : joi.number().integer().min(1).max(100).default(10),
  offset: joi.number().integer().min(0).default(0)
});

const id = joi.object().keys({
  id: joi.number().integer().min(1).required()
});

module.exports = {
  create,
  update,
  getList,
  id
};
