const _ = require('lodash');
const database = require('../../services/database');
const sqlManager = require('../../services/sqlManager');

module.exports = {
  create: async params => {
    const {
      author,
      createdBy,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    } = params;

    const sqlQuery = sqlManager.getSql(__dirname, 'create');
    const result = await database.query(sqlQuery, [
      author,
      createdBy,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    ]);

    return _.get(result, 'rows[0]', {});
  },

  update: async params => {
    const {
      id,
      author,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    } = params;

    const sqlQuery = sqlManager.getSql(__dirname, 'update');
    const result = await database.query(sqlQuery, [
      id,
      author,
      currency,
      description,
      price,
      propertyId,
      score,
      title,
      date,
      options
    ]);

    return _.get(result, 'rows[0]', {});
  },

  remove: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'delete');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  getOne: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getOne');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  getList: async options => {
    const {
      limit,
      offset
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getList');
    const result = await database.query(sqlQuery, [limit, offset]);

    return result.rows;
  },

  getOrderedList: async options => {
    const {
      userId,
      limit,
      offset
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getOrderedList');
    const result = await database.query(sqlQuery, [
      limit,
      offset,
      userId
    ]);

    return result.rows;
  },

  count: async () => {
    const sqlQuery = sqlManager.getSql(__dirname, 'count');
    const result = await database.query(sqlQuery, []);

    return _.get(result, 'rows[0].count', 0);
  },

  getListForProperty: async (options) => {
    const {
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getListForProperty');
    const result = await database.query(sqlQuery, [propertyId]);

    return result.rows;
  }
};
