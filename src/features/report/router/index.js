const express = require('express');
const utils = require('../../../utils');
const schemas = require('../report.schema');
const { ROLES } = require('../../../constants');
const accessForRoles = require('../../account/middleware/accessForRoles');

const {
  middleware: {
    validateRequest
  }
} = utils;

const router = express.Router();

router.post('/',
  accessForRoles([ROLES.ADMIN]),
  validateRequest(schemas.create, 'body'),
  require('../methods/create'));

router.get('/',
  accessForRoles([ROLES.ADMIN]),
  validateRequest(schemas.getList, 'query'),
  require('../methods/getList'));

router.get('/count',
  accessForRoles([ROLES.ADMIN]),
  require('../methods/count'));

router.get('/ordered',
  validateRequest(schemas.getList, 'query'),
  require('../methods/getOrderedList'));

router.put('/:id',
  accessForRoles([ROLES.ADMIN]),
  validateRequest(schemas.id, 'params'),
  validateRequest(schemas.update, 'body'),
  require('../methods/update'));

router.delete('/:id',
  accessForRoles([ROLES.ADMIN]),
  validateRequest(schemas.id, 'params'),
  require('../methods/delete'));

router.get('/:id',
  accessForRoles([ROLES.ADMIN]),
  validateRequest(schemas.id, 'params'),
  require('../methods/getOne'));

module.exports = router;
