INSERT INTO terms_ip_address
  ( "userId",
    "ipAddress" )
VALUES ($1, $2)
    RETURNING
    "id",
    "userId",
    "ipAddress",
    "createdAt";
