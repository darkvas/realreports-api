const _ = require('lodash');
const database = require('../../services/database');
const sqlManager = require('../../services/sqlManager');

module.exports = {
  create: async options => {
    const {
      userId,
      ipAddress
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'create');
    const result = await database.query(sqlQuery, [
      userId,
      ipAddress
    ]);

    return _.get(result, 'rows[0]', {});
  }
};
