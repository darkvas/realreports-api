const _ = require('lodash');
const requestIp = require('request-ip');
const orderModel = require('../order.model');
const reportModel = require('../../report/report.model');
const propertyModel = require('../../property/property.model');
const termsIpAddressModel = require('../../termsConditions/termsIpAddress.model');
const stripeService = require('../stripe.service');
const emailHandler = require('../../email/email.handler');
const { fileS3Service } = require('../../../services/aws');
const {
  responseSender,
  logger,
  customError: {
    BadRequestError
  }
} = require('../../../utils');

const getBufferFromStream = readStream => {
  return new Promise((resolve, reject) => {
    const chunks = [];

    readStream.once('error', err => {
      reject(err);
    });

    readStream.once('end', () => {
      const resultBuffer = Buffer.concat(chunks);
      resolve(resultBuffer);
    });

    readStream.on('data', chunk => {
      chunks.push(chunk);
    });
  });
};

const sendBoughtReportEmails = async (property, report, agent, user) => {
  try {
    const reportFileStream = fileS3Service.getReadStream({
      key        : report.reportFile.name,
      contentType: report.reportFile.contentType
    });

    const buffer = await getBufferFromStream(reportFileStream);

    const reportFileInfo = {
      name       : report.reportFile.name,
      contentType: report.reportFile.contentType,
      buffer
    };

    await emailHandler.notifyBoughtReport(property, reportFileInfo, agent, user);

    await emailHandler.notifySoldReport(property, agent, user);
  } catch (err) {
    logger.error(err);
  }
};

const storeIpAddress = (userId, req) => {
  const ipAddress = requestIp.getClientIp(req);
  return termsIpAddressModel.create({ userId, ipAddress });
};

module.exports = async (req, res, next) => {
  try {
    const {
      reportId,
      token
    } = res.locals.body;

    const userId = res.locals.user.id;

    const report = await reportModel.getOne({ id: reportId });
    if (!report || !report.id) {
      return next(new BadRequestError(`Not found report with: ${reportId}`));
    }

    const property = await propertyModel.getOne({ id: report.propertyId });
    if (!property || !property.id) {
      return next(new BadRequestError(`Not found property with id: ${report.propertyId}`));
    }

    const agent = _.get(property, 'agents[0]', null);
    if (!agent) {
      return next(new BadRequestError(`Not found agent for property with id: ${report.propertyId}`));
    }

    const previousOrder = await orderModel.getByUserReport({ userId, reportId });
    if (previousOrder && previousOrder.id) {
      return next(new BadRequestError('This report was already bought by user'));
    }

    await storeIpAddress(userId, req);

    const paymentInfo = await stripeService.makePayment({
      token,
      amount     : report.price,
      currency   : report.currency,
      reference  : report.id,
      description: `Order for report id: ${report.id}`
    });

    const order = await orderModel.create({
      userId,
      reportId : report.id,
      paydockId: paymentInfo.id,
      price    : report.price,
      currency : report.currency
    });

    // send emails asynchronously
    sendBoughtReportEmails(property, report, agent, res.locals.user);

    return responseSender.sendSuccess(res, order);
  } catch (ex) {
    return next(ex);
  }
};
