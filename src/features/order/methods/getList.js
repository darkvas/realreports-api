const model = require('../order.model');
const utils = require('../../../utils');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      limit,
      offset
    } = res.locals.query;

    const items = await model.getList({
      limit,
      offset
    });

    return responseSender.sendSuccess(res, { items });
  } catch (ex) {
    return next(ex);
  }
};
