const config = require('../../../../config');
const { responseSender } = require('../../../utils');

module.exports = (req, res, next) => {
  try {
    const paymentServiceId = config.paydock.stripeId;

    return responseSender.sendSuccess(res, {
      paymentServiceId
    });
  } catch (ex) {
    return next(ex);
  }
};
