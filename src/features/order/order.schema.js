const joi = require('joi');
const { DEFAULT_VALUES } = require('../../constants');

const create = joi.object().keys({
  token   : joi.string().min(1).required(),
  reportId: joi.number().integer().min(1).required()
});

const getList = joi.object().keys({
  limit : joi.number().integer().min(1).max(100).default(DEFAULT_VALUES.LIMIT),
  offset: joi.number().integer().min(0).default(DEFAULT_VALUES.OFFSET)
});

const id = joi.object().keys({
  id: joi.number().integer().min(1).required()
});

module.exports = {
  create,
  getList,
  id
};
