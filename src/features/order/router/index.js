const express = require('express');
const utils = require('../../../utils');
const schemas = require('../order.schema');
const accessForRoles = require('../../account/middleware/accessForRoles');
const { ROLES } = require('../../../constants');

const {
  middleware: {
    validateRequest
  }
} = utils;

const router = express.Router();

router.post('/',
  validateRequest(schemas.create, 'body'),
  require('../methods/create'));

router.get('/',
  validateRequest(schemas.getList, 'query'),
  accessForRoles([ROLES.ADMIN]),
  require('../methods/getList'));

router.get('/count',
  accessForRoles([ROLES.ADMIN]),
  require('../methods/count'));

router.get('/paymentServiceId',
  require('../methods/getPaymentServiceId'));

module.exports = router;
