SELECT
  orders.id,
  orders."userId",
  orders."reportId",
  orders."paydockId",
  orders."createdAt"
FROM orders
WHERE orders."userId" = $1
  AND orders."reportId" = $2
