INSERT INTO orders
  ( "reportId",
    "userId",
    "paydockId",
    "price",
    "currency")
VALUES ($1, $2, $3, $4, $5)
    RETURNING "id",
              "reportId",
              "userId",
              "paydockId",
              "price",
              "currency";
