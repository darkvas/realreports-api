SELECT
  orders.id,
  orders."userId",
  orders."reportId",
  orders."paydockId",
  orders."price",
  orders."currency",
  json_build_object(
    'id', reports.id,
    'title', reports."title",
    'description', reports."description",
    'score', reports."score",
    'price', reports."price",
    'currency', reports."currency",
    'date', reports."date"
  ) as report
FROM orders
  INNER JOIN reports ON reports.id = orders."reportId"
ORDER BY orders."id" DESC
  LIMIT $1 OFFSET $2;
