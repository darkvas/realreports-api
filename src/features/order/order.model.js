const _ = require('lodash');
const database = require('../../services/database');
const sqlManager = require('../../services/sqlManager');

module.exports = {
  create: async options => {
    const {
      reportId,
      userId,
      paydockId,
      price,
      currency
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'create');
    const result = await database.query(sqlQuery, [
      reportId,
      userId,
      paydockId,
      price,
      currency
    ]);

    return _.get(result, 'rows[0]', {});
  },

  getByUserReport: async ({ userId, reportId }) => {
    const sqlQuery = sqlManager.getSql(__dirname, 'getByUserReport');
    const result = await database.query(sqlQuery, [userId, reportId]);

    return _.get(result, 'rows[0]', {});
  },

  getList: async (options) => {
    const {
      limit,
      offset
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getList');
    const result = await database.query(sqlQuery, [limit, offset]);

    return result.rows;
  },

  count: async () => {
    const sqlQuery = sqlManager.getSql(__dirname, 'count');
    const result = await database.query(sqlQuery, []);

    return _.get(result, 'rows[0].count', 0);
  }
};
