const Stripe = require('stripe');
const config = require('../../../config');
const {
  customError: {
    BadRequestError
  }
} = require('../../utils');

const stripe = Stripe(config.stripe.secretKey);

const STRIPE_AMOUNT_MULTIPLIER = 100; // stripe uses values in cents

const handleError = ex => {
  if (ex.message && ex.type) {
    throw new BadRequestError(ex.message);
  } else {
    throw ex;
  }
};

module.exports = {
  async makePayment(options) {
    const {
      token,
      amount,
      currency,
      description
    } = options;

    try {
      const chargeOptions = {
        card  : token,
        amount: amount * STRIPE_AMOUNT_MULTIPLIER,
        currency, // ISO 4217
        description
      };

      const result = await stripe.charges.create(chargeOptions);

      return result;
    } catch (ex) {
      return handleError(ex);
    }
  }
};
