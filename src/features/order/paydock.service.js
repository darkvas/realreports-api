const request = require('request-promise-native');
const _ = require('lodash');
const config = require('../../../config');
const {
  customError: {
    BadRequestError
  }
} = require('../../utils');

const URL = 'https://api-sandbox.paydock.com/v1/charges?capture=true';

const handleError = response => {
  const message = _.get(response, 'error.message');
  throw new BadRequestError(message);
};

module.exports = {
  async makePayment(options) {
    const requestOptions = {
      method : 'POST',
      uri    : URL,
      body   : options,
      json   : true,
      simple : false,
      headers: {
        'x-user-secret-key': config.paydock.secretKey
      }
    };

    const response = await request(requestOptions);

    if (!(response.status === 200 || response.status === 201)) {
      return handleError(response);
    }

    const result = _.get(response, 'resource.data', null);
    if (!result) {
      throw new BadRequestError(response.resource);
    }

    return result;
  }
};
