const generateImageLink = require('../../image/utils/generateImageLink');

module.exports = inspector => {
  if (inspector.image && inspector.image.id) {
    inspector.image = {
      id  : inspector.image.id,
      name: inspector.image.name,
      href: generateImageLink(inspector.image)
    };
  } else {
    inspector.image = null;
  }
};
