const joi = require('joi');
const phoneJoi = joi.extend(require('joi-phone-number'));
const { DEFAULT_VALUES } = require('../../constants');

const create = phoneJoi.object().keys({
  firstName  : phoneJoi.string().min(1).max(100).required(),
  lastName   : phoneJoi.string().min(1).max(100).required(),
  phoneNumber: phoneJoi.string().phoneNumber({ defaultCountry: DEFAULT_VALUES.PHONE_COUNTRY, format: DEFAULT_VALUES.PHONE_FORMAT }).required(),
  email      : phoneJoi.string().email().required(),
  options    : phoneJoi.array().items(joi.string().required()).min(1).required()
});

const update = create.append({}); // make copy of create

const getList = joi.object().keys({
  limit : joi.number().integer().min(1).max(100).default(DEFAULT_VALUES.LIMIT),
  offset: joi.number().integer().min(0).default(DEFAULT_VALUES.OFFSET)
});

const id = joi.object().keys({
  id: joi.number().integer().min(1).required()
});

const propertyId = joi.object().keys({
  id        : joi.number().integer().min(1).required(),
  propertyId: joi.number().integer().min(1).required()
});

module.exports = {
  create,
  update,
  getList,
  id,
  propertyId
};
