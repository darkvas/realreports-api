UPDATE inspectors
  SET "firstName" = $2,
      "lastName" = $3,
      "phoneNumber" = $4,
      "email" = $5,
      "options" = $6,
      "updatedAt" = NOW()
  WHERE id = $1
    AND "deletedAt" is null
  RETURNING "id",
            "firstName",
            "lastName",
            "phoneNumber",
            "email",
            "options",
            "createdBy";
