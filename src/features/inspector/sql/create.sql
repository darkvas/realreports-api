INSERT INTO inspectors
  ( "firstName",
    "lastName",
    "phoneNumber",
    "email",
    "options",
    "createdBy")
VALUES ($1, $2, $3, $4, $5, $6)
RETURNING "id",
          "firstName",
          "lastName",
          "phoneNumber",
          "email",
          "options",
          "createdBy";
