INSERT INTO inspectors_properties
  ("inspectorId",
   "propertyId")
VALUES ($1, $2)
RETURNING
  "id",
  "inspectorId",
  "propertyId";
