UPDATE inspectors
  SET "deletedAt" = NOW()
  WHERE id = $1
    AND "deletedAt" is null
  RETURNING "id";
