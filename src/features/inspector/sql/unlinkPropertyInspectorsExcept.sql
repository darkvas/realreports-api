DELETE
FROM inspectors_properties
WHERE "id" != $1
  AND "propertyId" = $2
RETURNING
  "id",
  "inspectorId",
  "propertyId";
