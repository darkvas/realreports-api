SELECT
  inspectors.id,
  inspectors."firstName",
  inspectors."lastName",
  inspectors."email",
  inspectors."phoneNumber",
  inspectors."options",
  inspectors."createdBy",
  json_build_object(
    'id', images.id,
    'name', images."name",
    'link', images."link",
    'referenceId', images."referenceId",
    'referenceTable', images."referenceTable"
  ) AS image
FROM inspectors
  LEFT JOIN images ON images."id" = (
    SELECT "id" FROM images
    WHERE images."referenceId" = inspectors."id" AND images."referenceTable" = 'inspectors'
    ORDER BY images."id" DESC
    LIMIT 1 )
WHERE inspectors."id" = $1
  AND inspectors."deletedAt" IS NULL
