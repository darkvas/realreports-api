DELETE
FROM inspectors_properties
WHERE "inspectorId" = $1
  AND "propertyId" = $2
RETURNING
  "id",
  "inspectorId",
  "propertyId";
