const _ = require('lodash');
const database = require('../../services/database');
const sqlManager = require('../../services/sqlManager');

module.exports = {
  create: async params => {
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      options,
      createdBy
    } = params;

    const sqlQuery = sqlManager.getSql(__dirname, 'create');
    const result = await database.query(sqlQuery, [
      firstName,
      lastName,
      phoneNumber,
      email,
      options,
      createdBy
    ]);

    return _.get(result, 'rows[0]', {});
  },

  update: async params => {
    const {
      id,
      firstName,
      lastName,
      phoneNumber,
      email,
      options
    } = params;

    const sqlQuery = sqlManager.getSql(__dirname, 'update');
    const result = await database.query(sqlQuery, [
      id,
      firstName,
      lastName,
      phoneNumber,
      email,
      options
    ]);

    return _.get(result, 'rows[0]', {});
  },

  remove: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'remove');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  getOne: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getOne');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  getList: async options => {
    const {
      limit,
      offset
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getList');
    const result = await database.query(sqlQuery, [limit, offset]);

    return result.rows;
  },

  count: async () => {
    const sqlQuery = sqlManager.getSql(__dirname, 'count');
    const result = await database.query(sqlQuery, []);

    return _.get(result, 'rows[0].count', 0);
  },

  linkProperty: async options => {
    const {
      inspectorId,
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'linkProperty');
    const result = await database.query(sqlQuery, [inspectorId, propertyId]);

    return _.get(result, 'rows[0]', {});
  },

  unlinkProperty: async options => {
    const {
      inspectorId,
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'unlinkProperty');
    const result = await database.query(sqlQuery, [inspectorId, propertyId]);

    return _.get(result, 'rows[0]', {});
  },

  unlinkPropertyInspectorsExcept: async options => {
    const {
      id,
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'unlinkPropertyInspectorsExcept');
    const result = await database.query(sqlQuery, [id, propertyId]);

    return _.get(result, 'rows[0]', {});
  }
};
