const model = require('../inspector.model');
const utils = require('../../../utils');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const count = await model.count();

    return responseSender.sendSuccess(res, { count });
  } catch (ex) {
    return next(ex);
  }
};
