const _ = require('lodash');
const model = require('../inspector.model');
const { responseSender } = require('../../../utils');
const { DEFAULT_VALUES } = require('../../../constants');

module.exports = async (req, res, next) => {
  try {
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      options
    } = res.locals.body;

    const createdBy = _.get(res, 'locals.user.id', DEFAULT_VALUES.CREATED_BY);

    const result = await model.create({
      firstName,
      lastName,
      phoneNumber,
      email,
      options,
      createdBy
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
