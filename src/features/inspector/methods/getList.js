const model = require('../inspector.model');
const utils = require('../../../utils');
const mapImagesForInspector = require('../utils/mapImagesForInspector');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      limit,
      offset
    } = res.locals.query;

    const items = await model.getList({
      limit,
      offset
    });

    items.forEach(mapImagesForInspector);

    return responseSender.sendSuccess(res, { items });
  } catch (ex) {
    return next(ex);
  }
};
