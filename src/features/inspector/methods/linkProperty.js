const model = require('../inspector.model');
const utils = require('../../../utils');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      id: inspectorId,
      propertyId
    } = res.locals.params;

    const result = await model.linkProperty({
      inspectorId,
      propertyId
    });

    await model.unlinkPropertyInspectorsExcept({
      id: result.id,
      propertyId
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
