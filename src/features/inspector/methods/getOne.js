const model = require('../inspector.model');
const utils = require('../../../utils');
const mapImagesForInspector = require('../utils/mapImagesForInspector');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const id = res.locals.params.id;

    const result = await model.getOne({ id });

    if (result && result.id) {
      mapImagesForInspector(result);
    }

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
