const model = require('../inspector.model');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      options
    } = res.locals.body;

    const id = res.locals.params.id;

    const result = await model.update({
      id,
      firstName,
      lastName,
      phoneNumber,
      email,
      options
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
