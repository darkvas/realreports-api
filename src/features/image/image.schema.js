const joi = require('joi');

const REFERENCE_TABLES = [
  'properties', 'agents', 'inspectors'
];

const create = joi.object().keys({
  referenceName: joi.string().valid(REFERENCE_TABLES).required(),
  referenceId  : joi.number().integer().min(1).required()
});

const createQuery = joi.object().keys({
  referenceLink: joi.string().valid(['agent', 'agency']).default(null)
});

const id = joi.object().keys({
  referenceName: joi.string().valid(REFERENCE_TABLES).required(),
  referenceId  : joi.number().integer().min(1).required(),
  id           : joi.number().integer().min(1).required()
});

module.exports = {
  create,
  createQuery,
  id
};
