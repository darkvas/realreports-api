const _ = require('lodash');
const database = require('../../../services/database');
const {
  customError: {
    BadRequestError
  }
} = require('../../../utils');

const getReference = async ({ referenceTable, referenceId }) => {
  const queryParams = {
    name  : 'get-image-reference',
    text  : `SELECT "id" FROM ${referenceTable} WHERE id = $1`,
    values: [referenceId]
  };

  const result = await database.query(queryParams);
  return _.get(result, 'rows[0]', {});
};

module.exports = async (req, res, next) => {
  try {
    const {
      referenceName: referenceTable,
      referenceId
    } = res.locals.params;

    const reference = await getReference({ referenceTable, referenceId });

    if (!reference || !reference.id) {
      return next(new BadRequestError(`Not found reference: ${referenceTable} with id: ${referenceId}`));
    }
    return next();
  } catch (ex) {
    return next(ex);
  }
};
