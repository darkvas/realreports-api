const crypto = require('crypto');
const multer = require('multer');
const multerS3 = require('multer-s3');

const HEX_VALUE_LENGTH = 16;
const HEX_CHARS_IN_BYTE = 2;

const randomValueHex = hexLength => crypto.randomBytes(Math.ceil(hexLength / HEX_CHARS_IN_BYTE)).toString('hex').slice(0, hexLength);

const generateKey = file => `${randomValueHex(HEX_VALUE_LENGTH)}-${file.originalname}`;

module.exports = (s3Service, requestFileName) => multer({
  storage: multerS3({
    s3         : s3Service.s3,
    bucket     : s3Service.bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key        : (req, file, cb) => {
      cb(null, generateKey(file));
    }
  })
}).single(requestFileName);
