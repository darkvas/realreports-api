const imageModel = require('../image.model');
const { imageS3Service } = require('../../../services/aws');
const {
  responseSender,
  logger
} = require('../../../utils');

const tryRemoveImageObject = async ({ key }) => {
  try {
    await imageS3Service.removeObject({ key });
  } catch (ex) {
    logger.error(ex);
  }
};

module.exports = async (req, res, next) => {
  try {
    const {
      referenceName: referenceTable,
      referenceId,
      id
    } = res.locals.params;

    const result = await imageModel.remove({
      referenceTable,
      referenceId,
      id
    });

    if (result && result.name) {
      // try remove from s3 and ignore if error
      await tryRemoveImageObject({ key: result.name });
    }

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
