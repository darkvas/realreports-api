const imageModel = require('../image.model');
const { imageS3Service } = require('../../../services/aws');
const {
  customError: {
    AwsError,
    NotFoundError
  }
} = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      referenceName: referenceTable,
      referenceId,
      id
    } = res.locals.params;

    const result = await imageModel.getOne({
      referenceTable,
      referenceId,
      id
    });

    if (!result || !result.name) {
      return next(new NotFoundError('Image not exist'));
    }

    res.setHeader('content-type', result.contentType);

    const imageStream = imageS3Service.getReadStream({
      key        : result.name,
      contentType: result.contentType
    });

    // TODO check error: Can't set headers after they are sent.
    return imageStream.on('error', err => {
      res.setHeader('content-type', 'application/json');
      return next(new AwsError(err));
    }).pipe(res);
  } catch (ex) {
    return next(ex);
  }
};
