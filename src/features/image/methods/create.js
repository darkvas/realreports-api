const imageModel = require('../image.model');
const { responseSender } = require('../../../utils');
const generateImageLink = require('../utils/generateImageLink');

module.exports = async (req, res, next) => {
  try {
    const {
      referenceName: referenceTable,
      referenceId
    } = res.locals.params;

    const {
      referenceLink
    } = res.locals.query;

    const {
      key     : name,
      location,
      contentType
    } = req.file;

    const link = referenceLink ? referenceLink : location;

    const result = await imageModel.create({
      referenceTable,
      referenceId,
      name,
      link,
      contentType
    });

    const href = generateImageLink(result);

    return responseSender.sendSuccess(res, { href });
  } catch (ex) {
    return next(ex);
  }
};
