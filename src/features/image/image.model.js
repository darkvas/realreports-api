const _ = require('lodash');
const database = require('../../services/database');
const sqlManager = require('../../services/sqlManager');

module.exports = {
  create: async options => {
    const {
      referenceTable,
      referenceId,
      name,
      link,
      contentType
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'create');
    const result = await database.query(sqlQuery, [
      referenceTable,
      referenceId,
      name,
      link,
      contentType
    ]);

    return _.get(result, 'rows[0]', {});
  },

  getOne: async options => {
    const {
      referenceTable,
      referenceId,
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getOne');
    const result = await database.query(sqlQuery, [
      referenceTable,
      referenceId,
      id
    ]);

    return _.get(result, 'rows[0]', {});
  },

  remove: async options => {
    const {
      referenceTable,
      referenceId,
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'remove');
    const result = await database.query(sqlQuery, [
      referenceTable,
      referenceId,
      id
    ]);

    return _.get(result, 'rows[0]', {});
  }
};
