SELECT
  "id",
  "referenceTable",
  "referenceId",
  "name",
  "link",
  "contentType"
FROM images
WHERE "referenceTable" = $1
  AND "referenceId" = $2
  AND "id" = $3
