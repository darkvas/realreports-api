INSERT INTO images
  (
    "referenceTable",
    "referenceId",
    "name",
    "link",
    "contentType"
  )
VALUES ($1, $2, $3, $4, $5)
RETURNING
  "id",
  "referenceTable",
  "referenceId",
  "name",
  "link",
  "contentType"
