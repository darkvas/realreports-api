DELETE
FROM images
WHERE "referenceTable" = $1
  AND "referenceId" = $2
  AND "id" = $3
RETURNING
  "id",
  "name";
