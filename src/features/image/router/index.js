const express = require('express');
const utils = require('../../../utils');
const schemas = require('../image.schema');
const { imageS3Service } = require('../../../services/aws');
const multipartS3Upload = require('../middleware/multipartS3Upload.middleware');
const checkReference = require('../middleware/checkReference.middleware');
const { ROLES } = require('../../../constants');
const authenticate = require('../../account/middleware/authenticate');
const accessForRoles = require('../../account/middleware/accessForRoles');

const {
  middleware: {
    validateRequest
  }
} = utils;

const REQUEST_FILENAME = 'image';

const router = express.Router();

router.post('/:referenceName/:referenceId',
  validateRequest(schemas.create, 'params'),
  validateRequest(schemas.createQuery, 'query'),
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  checkReference,
  multipartS3Upload(imageS3Service, REQUEST_FILENAME),
  require('../methods/create'));

router.get('/:referenceName/:referenceId/:id',
  validateRequest(schemas.id, 'params'),
  require('../methods/getOne'));

router.delete('/:referenceName/:referenceId/:id',
  validateRequest(schemas.id, 'params'),
  authenticate,
  accessForRoles([ROLES.ADMIN]),
  require('../methods/delete'));

module.exports = router;
