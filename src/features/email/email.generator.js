const fs = require('fs');
const path = require('path');
const util = require('util');
const _ = require('lodash');

const TEMPLATE_FOLDER = 'templates';

module.exports = {
  async generateContentForTemplate(meta, payload) {
    const readFile = util.promisify(fs.readFile);

    const templateFilePath = path.join(__dirname, TEMPLATE_FOLDER, meta.template, 'index.html');
    const templateContent = await readFile(templateFilePath, 'utf8');

    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g; // use mustache template delimiters
    const compiled = _.template(templateContent);
    const bodyContent = compiled(payload);

    return {
      emailTo: meta.emailTo,
      subject: meta.subject,
      body   : bodyContent
    };
  }
};
