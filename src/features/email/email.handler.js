const config = require('../../../config');
const { emailService } = require('../../services/mandrill');
const getFullAddress = require('../property/utils/getFullAddress');
const getPropertyFirstImageLink = require('../property/utils/getPropertyFirstImageLink');
const { logger } = require('../../utils');
const emailGenerator = require('./email.generator');

const sendEmail = async emailOptions => {
  const result = await emailService.sendEmail(emailOptions);
  logger.info(`Email sent to: ${emailOptions.emailTo}, with message id: ${result && result.MessageId}`);
  return result;
};

module.exports = {
  async notifyRequestReport(property, agent, user) {
    const emailOptions = await emailGenerator.generateContentForTemplate(
      {
        emailTo : agent.email,
        subject : 'User requested report for property',
        template: 'requestReport'
      },
      {
        propertyAddress: getFullAddress(property),
        propertyLink   : `${config.aws.publicUrl}/properties/${property.id}`,
        userEmail      : user.email,
        userPhoneNumber: user.phoneNumber,
        userFullName   : `${user.firstName} ${user.lastName}`
      }
    );

    return sendEmail(emailOptions);
  },

  async notifyRequestReportToUser(property, agent, user) {
    const emailOptions = await emailGenerator.generateContentForTemplate(
      {
        emailTo : user.email,
        subject : `${getFullAddress(property)}`,
        template: 'requestReportToUser'
      },
      {
        propertyAddress: getFullAddress(property),
        propertyLink   : `${config.aws.publicUrl}/properties/${property.id}`,
        userEmail      : user.email,
        userPhoneNumber: user.phoneNumber,
        userFullName   : `${user.firstName} ${user.lastName}`,
        agentName      : `${agent.firstName} ${agent.lastName}`,
        agentContact   : agent.phoneNumber,
        agencyTitle    : agent.agencyTitle
      }
    );

    return sendEmail(emailOptions);
  },

  async notifyBoughtReport(property, reportFileInfo, agent, user) {
    const emailOptions = await emailGenerator.generateContentForTemplate(
      {
        emailTo : user.email,
        subject : `Thank you for your purchase of a report for ${getFullAddress(property)} from Real Reports`,
        template: 'boughtReport'
      },
      {
        propertyAddress  : getFullAddress(property),
        propertyLink     : `${config.aws.publicUrl}/properties/${property.id}`,
        propertyImageLink: getPropertyFirstImageLink(property),
        userFullName     : `${user.firstName} ${user.lastName}`
      }
    );

    emailOptions.attachments = [
      {
        type   : reportFileInfo.contentType,
        name   : 'PropertyReport.pdf', // reportFileInfo.name,
        content: reportFileInfo.buffer.toString('base64')
      }
    ];

    return sendEmail(emailOptions);
  },

  async notifySoldReport(property, agent, user) {
    const emailOptions = await emailGenerator.generateContentForTemplate(
      {
        emailTo : agent.email,
        subject : `A report was just purchased for ${getFullAddress(property)} from Real Reports`,
        template: 'soldReport'
      },
      {
        propertyAddress  : getFullAddress(property),
        propertyLink     : `${config.aws.publicUrl}/properties/${property.id}`,
        propertyImageLink: getPropertyFirstImageLink(property),
        userEmail        : user.email,
        userPhoneNumber  : user.phoneNumber,
        userFullName     : `${user.firstName} ${user.lastName}`,
        agentName        : agent.firstName
      }
    );

    return sendEmail(emailOptions);
  },

  async notifyRequestProperty(propertyInfo, agent, user) {
    const emailOptions = await emailGenerator.generateContentForTemplate(
      {
        emailTo : agent.email,
        subject : 'User requested property info',
        template: 'requestProperty'
      },
      {
        propertyAddress: propertyInfo.searchAddress,
        userEmail      : user.email,
        userPhoneNumber: user.phoneNumber,
        userFullName   : `${user.firstName} ${user.lastName}`,
        agentName      : propertyInfo.agentName,
        agentContact   : propertyInfo.agentContact,
        agencyTitle    : propertyInfo.agencyTitle
      }
    );

    return sendEmail(emailOptions);
  }
};
