const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const accessToken = res.locals.accessToken;
    const {
      firstName,
      lastName
    } = res.locals.body;

    await authCognitoService.updateProfile({
      accessToken,
      firstName,
      lastName
    });

    return responseSender.sendSuccess(res, { firstName, lastName });
  } catch (ex) {
    return next(ex);
  }
};
