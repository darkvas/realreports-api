const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const accessToken = res.locals.accessToken;
    const {
      oldPassword,
      newPassword
    } = res.locals.body;

    await authCognitoService.updatePassword({
      accessToken,
      oldPassword,
      newPassword
    });

    return responseSender.sendSuccess(res, {});
  } catch (ex) {
    return next(ex);
  }
};
