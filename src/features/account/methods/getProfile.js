const { responseSender } = require('../../../utils');

module.exports = (req, res, next) => {
  try {
    const {
      email,
      firstName,
      lastName,
      phoneNumber,
      phoneNumberVerified,
      role
    } = res.locals.user;

    return responseSender.sendSuccess(res, {
      email,
      firstName,
      lastName,
      phoneNumber,
      phoneNumberVerified,
      role
    });
  } catch (ex) {
    return next(ex);
  }
};
