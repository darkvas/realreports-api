const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const accessToken = res.locals.accessToken;

    const result = await authCognitoService.signOut({
      accessToken
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
