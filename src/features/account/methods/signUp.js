const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');
const { ROLES } = require('../../../constants');

module.exports = async (req, res, next) => {
  try {
    const {
      email,
      password,
      firstName,
      lastName,
      phoneNumber
    } = res.locals.body;

    const result = await authCognitoService.signUp({
      email,
      password,
      firstName,
      lastName,
      phoneNumber,
      role: ROLES.BUYER
    });

    return responseSender.sendSuccess(res, result);

  } catch (ex) {
    return next(ex);
  }
};
