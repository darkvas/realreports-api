const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      refreshToken
    } = res.locals.body;

    const result = await authCognitoService.refreshToken({
      refreshToken
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
