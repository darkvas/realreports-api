const { authCognitoService } = require('../../../services/aws');
const { responseSender } = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      email,
      password,
      confirmationCode
    } = res.locals.body;

    const result = await authCognitoService.confirmForgotPassword({
      email,
      password,
      confirmationCode
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
