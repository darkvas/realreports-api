const { authCognitoService } = require('../../../services/aws/index');
const { ROLES } = require('../../../constants/index');
const {
  logger
} = require('../../../utils/index');

const adminData = require('./data/defaultAdmin.json');

adminData.role = ROLES.ADMIN;

const ERR_USER_NOT_FOUND = 'UserNotFoundException';

const tryGetUserByEmail = async email => {
  try {
    const result = await authCognitoService.adminGetUser({ id: email }); // email could be used as id
    return result;
  } catch (ex) {
    if (ex.name === ERR_USER_NOT_FOUND) {
      return null;
    }
    throw ex;
  }
};

module.exports = async () => {
  try {
    const adminUser = await tryGetUserByEmail(adminData.email);

    if (adminUser) {
      return logger.info(`Admin is exist: ${adminUser.email}`);
    }

    await authCognitoService.signUp(adminData);
    await authCognitoService.adminConfirmSignUp({ email: adminData.email });

    logger.info(`Admin is created: ${adminData.email}`);
  } catch (ex) {
    logger.error(ex);
  }
};
