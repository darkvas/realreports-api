const { authCognitoService } = require('../../../services/aws');
const {
  customError: {
    AwsError,
    UnauthorizedError
  }
} = require('../../../utils');

const TOKEN_NAME = 'x-access-token';
const AWS_UNAUTHORIZED_ERROR = 'NotAuthorizedException';

const handleAwsUnAuthError = err => {
  if (err.name === AWS_UNAUTHORIZED_ERROR) {
    return new UnauthorizedError();
  }
  return new AwsError(err);
};

module.exports = async (req, res, next) => {
  const accessToken = req.headers[TOKEN_NAME];

  if (!accessToken) {
    return next(new UnauthorizedError());
  }

  try {
    const userData = await authCognitoService.getUser({ accessToken });

    res.locals.accessToken = accessToken;
    res.locals.user = userData;
    return next();
  } catch (ex) {
    return next(handleAwsUnAuthError(ex));
  }
};
