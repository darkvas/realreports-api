const { ROLES } = require('../../../constants');
const {
  customError: {
    ForbiddenError,
    UnauthorizedError
  }
} = require('../../../utils');

const validRoles = [ROLES.ADMIN, ROLES.BUYER];

module.exports = allowedRoles => {
  if (!Array.isArray(allowedRoles)) {
    throw new Error('"allowedRoles" parameter should be type of Array');
  }

  if (!allowedRoles.every(item => validRoles.includes(item))) {
    throw new Error(`"allowedRoles" parameter should be in: ${validRoles}`);
  }

  return function middleware(req, res, next) {
    try {
      if (!res.locals || !res.locals.user) {
        return next(new UnauthorizedError());
      }

      const role = res.locals.user.role;

      if (!allowedRoles.includes(role)) {
        return next(new ForbiddenError());
      }

      return next();
    } catch (ex) {
      return next(ex);
    }
  };
};
