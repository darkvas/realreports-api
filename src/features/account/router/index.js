const express = require('express');
const utils = require('../../../utils');
const schemas = require('../account.schema');
const generateDefaultAdmin = require('../scripts/generateDefaultAdmin');

const {
  middleware: {
    validateRequest
  }
} = utils;

generateDefaultAdmin();

const router = express.Router();

router.post('/signUp',
  validateRequest(schemas.signUp, 'body'),
  require('../methods/signUp'));

router.post('/confirmPhone',
  validateRequest(schemas.confirmPhone, 'body'),
  require('../methods/confirmPhone'));

router.post('/signIn',
  validateRequest(schemas.signIn, 'body'),
  require('../methods/signIn'));

router.post('/signOut',
  require('../middleware/authenticate'),
  require('../methods/signOut'));

router.post('/refreshToken',
  validateRequest(schemas.refreshToken, 'body'),
  require('../methods/refreshToken'));

router.get('/profile',
  require('../middleware/authenticate'),
  require('../methods/getProfile'));

router.put('/profile',
  validateRequest(schemas.profile, 'body'),
  require('../middleware/authenticate'),
  require('../methods/updateProfile'));

router.put('/password',
  validateRequest(schemas.updatePassword, 'body'),
  require('../middleware/authenticate'),
  require('../methods/updatePassword'));

router.post('/forgotPassword',
  validateRequest(schemas.forgotPasswordRequest, 'body'),
  require('../methods/forgotPasswordRequest'));

router.post('/forgotPasswordConfirm',
  validateRequest(schemas.forgotPasswordConfirm, 'body'),
  require('../methods/forgotPasswordConfirm'));

module.exports = router;
