const Joi = require('joi');
const phoneJoi = Joi.extend(require('joi-phone-number'));
const {
  REGEX_PASS,
  REGEX_PASS_OPTIONS,
  PASS_CUSTOM_MESSAGE
} = require('../../constants').VALIDATIONS;

const signUp = phoneJoi.object().keys({
  email      : phoneJoi.string().email().required(),
  password   : phoneJoi.string().min(8).max(32).regex(REGEX_PASS, REGEX_PASS_OPTIONS).required().error(() => PASS_CUSTOM_MESSAGE),
  firstName  : phoneJoi.string().min(1).max(100).required(),
  lastName   : phoneJoi.string().min(1).max(100).required(),
  phoneNumber: phoneJoi.string().phoneNumber({ defaultCountry: 'AU', format: 'e164' }).required()
});

const signIn = phoneJoi.object().keys({
  email   : phoneJoi.string().email().required(),
  password: phoneJoi.string().min(8).max(32).regex(REGEX_PASS, REGEX_PASS_OPTIONS).required().error(() => PASS_CUSTOM_MESSAGE)
});

const confirmPhone = phoneJoi.object().keys({
  email           : phoneJoi.string().email().required(),
  confirmationCode: phoneJoi.string().min(2).max(10).regex(/^[0-9]{2,10}$/, { name: 'numbers' }).required()
});

const profile = phoneJoi.object().keys({
  firstName: phoneJoi.string().min(1).max(100).required(),
  lastName : phoneJoi.string().min(1).max(100).required()
});

const refreshToken = phoneJoi.object().keys({
  refreshToken: phoneJoi.string().min(20).max(2000).required()
});

const updatePassword = phoneJoi.object().keys({
  oldPassword: phoneJoi.string().min(8).max(32).regex(REGEX_PASS, REGEX_PASS_OPTIONS).required().error(() => PASS_CUSTOM_MESSAGE),
  newPassword: phoneJoi.string().min(8).max(32).regex(REGEX_PASS, REGEX_PASS_OPTIONS).required().error(() => PASS_CUSTOM_MESSAGE)
});

const forgotPasswordRequest = phoneJoi.object().keys({
  email: phoneJoi.string().email().required()
});

const forgotPasswordConfirm = phoneJoi.object().keys({
  email           : phoneJoi.string().email().required(),
  password        : phoneJoi.string().min(8).max(32).regex(REGEX_PASS, REGEX_PASS_OPTIONS).required().error(() => PASS_CUSTOM_MESSAGE),
  confirmationCode: phoneJoi.string().min(2).max(10).regex(/^[0-9]{2,10}$/, { name: 'numbers' }).required()
});

module.exports = {
  signUp,
  signIn,
  confirmPhone,
  profile,
  refreshToken,
  updatePassword,
  forgotPasswordRequest,
  forgotPasswordConfirm
};
