const express = require('express');
const utils = require('../../../utils');
const schemas = require('../agent.schema');

const {
  middleware: {
    validateRequest
  }
} = utils;

const router = express.Router();

router.post('/',
  validateRequest(schemas.create, 'body'),
  require('../methods/create'));

router.get('/',
  validateRequest(schemas.getList, 'query'),
  require('../methods/getList'));

router.get('/count',
  require('../methods/count'));

router.post('/:id/property/:propertyId',
  validateRequest(schemas.propertyId, 'params'),
  require('../methods/linkProperty'));

router.delete('/:id/property/:propertyId',
  validateRequest(schemas.propertyId, 'params'),
  require('../methods/unlinkProperty'));

router.put('/:id',
  validateRequest(schemas.id, 'params'),
  validateRequest(schemas.update, 'body'),
  require('../methods/update'));

router.delete('/:id',
  validateRequest(schemas.id, 'params'),
  require('../methods/remove'));

router.get('/:id',
  validateRequest(schemas.id, 'params'),
  require('../methods/getOne'));

module.exports = router;
