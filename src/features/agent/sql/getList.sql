SELECT
  agents.id,
  agents."firstName",
  agents."lastName",
  agents."email",
  agents."phoneNumber",
  agents."agencyTitle",
  agents."createdBy",
  json_agg(json_build_object(
    'id', images.id,
    'name', images."name",
    'link', images."link",
    'referenceId', images."referenceId",
    'referenceTable', images."referenceTable"
  )) AS images
FROM agents
  LEFT JOIN images ON images."referenceId" = agents.id AND images."referenceTable" = 'agents'
  LEFT JOIN agents_properties ON agents_properties."agentId" = agents.Id
WHERE agents."deletedAt" IS NULL
  AND ($3::integer IS NULL OR agents_properties."propertyId" = $3)
  GROUP BY
  agents.id,
  agents."firstName",
  agents."lastName",
  agents."email",
  agents."phoneNumber",
  agents."agencyTitle",
  agents."createdBy"
  ORDER BY agents."id" DESC
    LIMIT $1 OFFSET $2;
