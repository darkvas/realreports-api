INSERT INTO agents_properties
  ("agentId",
   "propertyId")
VALUES ($1, $2)
RETURNING
  "id",
  "agentId",
  "propertyId";
