SELECT
  agents.id,
  agents."firstName",
  agents."lastName",
  agents."email",
  agents."phoneNumber",
  agents."agencyTitle",
  agents."createdBy",
  json_agg(json_build_object(
    'id', images.id,
    'name', images."name",
    'link', images."link",
    'referenceId', images."referenceId",
    'referenceTable', images."referenceTable"
  )) AS images
FROM agents
LEFT JOIN images ON images."referenceId" = agents.id AND images."referenceTable" = 'agents'
WHERE agents."id" = $1
  AND agents."deletedAt" IS NULL
  GROUP BY
  agents.id,
  agents."firstName",
  agents."lastName",
  agents."email",
  agents."phoneNumber",
  agents."agencyTitle",
  agents."createdBy"
