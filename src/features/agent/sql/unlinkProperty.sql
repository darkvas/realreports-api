DELETE
FROM agents_properties
WHERE "agentId" = $1
  AND "propertyId" = $2
RETURNING
  "id",
  "agentId",
  "propertyId";
