INSERT INTO agents
  ( "firstName",
    "lastName",
    "phoneNumber",
    "email",
    "agencyTitle",
    "createdBy")
VALUES ($1, $2, $3, $4, $5, $6)
RETURNING "id",
          "firstName",
          "lastName",
          "phoneNumber",
          "email",
          "agencyTitle",
          "createdBy";
