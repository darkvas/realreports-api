DELETE
FROM agents_properties
WHERE "id" != $1
  AND "propertyId" = $2
RETURNING
  "id",
  "agentId",
  "propertyId";
