UPDATE agents
  SET "firstName" = $2,
      "lastName" = $3,
      "phoneNumber" = $4,
      "email" = $5,
      "agencyTitle" = $6,
      "updatedAt" = NOW()
  WHERE id = $1
    AND "deletedAt" is null
  RETURNING "id",
            "firstName",
            "lastName",
            "phoneNumber",
            "email",
            "agencyTitle",
            "createdBy";
