const _ = require('lodash');
const database = require('../../services/database');
const sqlManager = require('../../services/sqlManager');

module.exports = {
  create: async options => {
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      agencyTitle,
      createdBy
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'create');
    const result = await database.query(sqlQuery, [
      firstName,
      lastName,
      phoneNumber,
      email,
      agencyTitle,
      createdBy
    ]);

    return _.get(result, 'rows[0]', {});
  },

  update: async options => {
    const {
      id,
      firstName,
      lastName,
      phoneNumber,
      email,
      agencyTitle
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'update');
    const result = await database.query(sqlQuery, [
      id,
      firstName,
      lastName,
      phoneNumber,
      email,
      agencyTitle
    ]);

    return _.get(result, 'rows[0]', {});
  },

  remove: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'remove');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  getOne: async options => {
    const {
      id
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getOne');
    const result = await database.query(sqlQuery, [id]);

    return _.get(result, 'rows[0]', {});
  },

  getList: async options => {
    const {
      limit,
      offset,
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'getList');
    const result = await database.query(sqlQuery, [
      limit, offset, propertyId
    ]);

    return result.rows;
  },

  linkProperty: async options => {
    const {
      agentId,
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'linkProperty');
    const result = await database.query(sqlQuery, [agentId, propertyId]);

    return _.get(result, 'rows[0]', {});
  },

  unlinkProperty: async options => {
    const {
      agentId,
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'unlinkProperty');
    const result = await database.query(sqlQuery, [agentId, propertyId]);

    return _.get(result, 'rows[0]', {});
  },

  count: async () => {
    const sqlQuery = sqlManager.getSql(__dirname, 'count');
    const result = await database.query(sqlQuery, []);

    return _.get(result, 'rows[0].count', 0);
  },

  unlinkPropertyAgentsExcept: async options => {
    const {
      id,
      propertyId
    } = options;

    const sqlQuery = sqlManager.getSql(__dirname, 'unlinkPropertyAgentsExcept');
    const result = await database.query(sqlQuery, [id, propertyId]);

    return _.get(result, 'rows[0]', {});
  }
};
