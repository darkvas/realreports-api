const model = require('../agent.model');
const utils = require('../../../utils');
const mapImagesForAgent = require('../utils/mapImagesForAgent');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const id = res.locals.params.id;

    const result = await model.getOne({ id });

    if (result && result.id) {
      mapImagesForAgent(result);
    }

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
