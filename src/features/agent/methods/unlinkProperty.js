const model = require('../agent.model');
const utils = require('../../../utils');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      id: agentId,
      propertyId
    } = res.locals.params;

    const result = await model.unlinkProperty({
      agentId,
      propertyId
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
