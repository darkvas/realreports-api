const model = require('../agent.model');
const utils = require('../../../utils');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      id: agentId,
      propertyId
    } = res.locals.params;

    const result = await model.linkProperty({
      agentId,
      propertyId
    });

    await model.unlinkPropertyAgentsExcept({
      id: result.id,
      propertyId
    });

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
