const model = require('../agent.model');
const utils = require('../../../utils');
const mapImagesForAgent = require('../utils/mapImagesForAgent');

const responseSender = utils.responseSender;

module.exports = async (req, res, next) => {
  try {
    const {
      limit,
      offset
    } = res.locals.query;

    const items = await model.getList({
      limit,
      offset
    });

    items.forEach(mapImagesForAgent);

    return responseSender.sendSuccess(res, { items });
  } catch (ex) {
    return next(ex);
  }
};
