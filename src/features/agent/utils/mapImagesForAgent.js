const generateImageLink = require('../../image/utils/generateImageLink');

const findLinkAndMap = (images, linkName) => {
  const linkImage = images.find(item => item.link === linkName);

  if (!linkImage) {
    return null;
  }

  return {
    id  : linkImage.id,
    name: linkImage.name,
    href: generateImageLink(linkImage)
  };
};

module.exports = agent => {
  agent.agentImage = findLinkAndMap(agent.images, 'agent');
  agent.agencyImage = findLinkAndMap(agent.images, 'agency');

  delete agent.images;
};
