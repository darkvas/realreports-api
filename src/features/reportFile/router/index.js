const express = require('express');
const utils = require('../../../utils');
const schemas = require('../reportFile.schema');
const { fileS3Service } = require('../../../services/aws');
const multipartS3Upload = require('../../image/middleware/multipartS3Upload.middleware');
const checkReference = require('../middleware/checkReference.middleware');
const { ROLES } = require('../../../constants');
const accessForRoles = require('../../account/middleware/accessForRoles');
const checkReportAccess = require('../middleware/checkReportAccess.middleware');

const {
  middleware: {
    validateRequest
  }
} = utils;

const REQUEST_FILENAME = 'file';

const router = express.Router();

router.post('/reports/:reportId',
  validateRequest(schemas.create, 'params'),
  accessForRoles([ROLES.ADMIN]),
  checkReference,
  multipartS3Upload(fileS3Service, REQUEST_FILENAME),
  require('../methods/create'));

router.get('/reports/:reportId/:id',
  validateRequest(schemas.id, 'params'),
  checkReportAccess,
  require('../methods/getOne'));

router.delete('/reports/:reportId/:id',
  validateRequest(schemas.id, 'params'),
  accessForRoles([ROLES.ADMIN]),
  require('../methods/delete'));

module.exports = router;
