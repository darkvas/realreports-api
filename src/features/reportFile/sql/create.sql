INSERT INTO report_files
  (
    "reportId",
    "name",
    "link",
    "contentType"
  )
VALUES ($1, $2, $3, $4)
RETURNING
  "id",
  "reportId",
  "name",
  "link",
  "contentType"
