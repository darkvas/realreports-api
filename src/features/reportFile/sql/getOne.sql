SELECT
  "id",
  "reportId",
  "name",
  "link",
  "contentType"
FROM report_files
WHERE "reportId" = $1
  AND "id" = $2
