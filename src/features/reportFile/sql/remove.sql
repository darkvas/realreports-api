DELETE
FROM report_files
WHERE "reportId" = $1
  AND "id" = $2
RETURNING
  "id",
  "name";
