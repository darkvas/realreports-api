const reportFileModel = require('../reportFile.model');
const { responseSender } = require('../../../utils');
const generateFileLink = require('../utils/generateFileLink');

module.exports = async (req, res, next) => {
  try {
    const {
      reportId
    } = res.locals.params;

    const {
      key     : name,
      location: link,
      contentType
    } = req.file;

    const result = await reportFileModel.create({
      reportId,
      name,
      link,
      contentType
    });

    const href = generateFileLink(result);

    return responseSender.sendSuccess(res, { href });
  } catch (ex) {
    return next(ex);
  }
};
