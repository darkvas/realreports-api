const reportFileModel = require('../reportFile.model');
const { fileS3Service } = require('../../../services/aws');
const {
  customError: {
    AwsError,
    NotFoundError
  }
} = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    const {
      reportId,
      id
    } = res.locals.params;

    const result = await reportFileModel.getOne({
      reportId,
      id
    });

    if (!result || !result.name) {
      return next(new NotFoundError('File not exist'));
    }

    res.setHeader('content-type', result.contentType);

    const imageStream = fileS3Service.getReadStream({
      key        : result.name,
      contentType: result.contentType
    });

    return imageStream.on('error', err => {
      res.setHeader('content-type', 'application/json');
      return next(new AwsError(err));
    }).pipe(res);
  } catch (ex) {
    return next(ex);
  }
};
