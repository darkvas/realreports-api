const reportFileModel = require('../reportFile.model');
const { fileS3Service } = require('../../../services/aws');
const {
  responseSender,
  logger
} = require('../../../utils');

const tryRemoveImageObject = async ({ key }) => {
  try {
    await fileS3Service.removeObject({ key });
  } catch (ex) {
    logger.error(ex);
  }
};

module.exports = async (req, res, next) => {
  try {
    const {
      reportId,
      id
    } = res.locals.params;

    const result = await reportFileModel.remove({
      reportId,
      id
    });

    if (result && result.name) {
      // try remove from s3 and ignore if error
      await tryRemoveImageObject({ key: result.name });
    }

    return responseSender.sendSuccess(res, result);
  } catch (ex) {
    return next(ex);
  }
};
