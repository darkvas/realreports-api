const config = require('../../../../config');

const PUBLIC_URL = config.aws.ec2PublicUrl;
const API_PREFIX = '/api/v1';
const FEATURE_PREFIX = '/files';

module.exports = options => {
  const {
    id,
    reportId
  } = options;

  return `${PUBLIC_URL}${API_PREFIX}${FEATURE_PREFIX}/reports/${reportId}/${id}`;
};
