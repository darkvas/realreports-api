const _ = require('lodash');
const database = require('../../../services/database');
const {
  customError: {
    BadRequestError
  }
} = require('../../../utils');

const getReference = async ({ referenceTable, referenceId }) => {
  const queryParams = {
    name  : 'get-file-reference',
    text  : `SELECT "id" FROM ${referenceTable} WHERE "id" = $1 AND "deletedAt" IS NULL`,
    values: [referenceId]
  };

  const result = await database.query(queryParams);
  return _.get(result, 'rows[0]', {});
};

module.exports = async (req, res, next) => {
  try {
    const {
      reportId
    } = res.locals.params;

    const referenceTable = 'reports';

    const reference = await getReference({ referenceTable, referenceId: reportId });

    if (!reference || !reference.id) {
      return next(new BadRequestError(`Not found reference: ${referenceTable} with id: ${reportId}`));
    }
    return next();
  } catch (ex) {
    return next(ex);
  }
};
