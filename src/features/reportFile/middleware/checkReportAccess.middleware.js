const { ROLES } = require('../../../constants');
const orderModel = require('../../order/order.model');
const {
  customError: {
    UnauthorizedError,
    ForbiddenError
  }
} = require('../../../utils');

module.exports = async (req, res, next) => {
  try {
    if (!res.locals || !res.locals.user) {
      return next(new UnauthorizedError());
    }

    const role = res.locals.user.role;

    if (role === ROLES.ADMIN) {
      return next();
    }

    const order = await orderModel.getByUserReport({
      reportId: res.locals.params.reportId,
      userId  : res.locals.user.id
    });

    if (order && order.id) {
      return next();
    }

    return next(new ForbiddenError());
  } catch (ex) {
    return next(ex);
  }
};
