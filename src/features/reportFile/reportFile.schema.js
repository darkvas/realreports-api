const joi = require('joi');

const create = joi.object().keys({
  reportId: joi.number().integer().min(1).required()
});

const id = joi.object().keys({
  reportId: joi.number().integer().min(1).required(),
  id      : joi.number().integer().min(1).required()
});

module.exports = {
  create,
  id
};
