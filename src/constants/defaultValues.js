module.exports = Object.freeze({
  CREATED_BY: '3404dc20-3bcb-43d5-8771-c4fe693d17d8',

  OFFSET: 0,
  LIMIT : 10,

  PHONE_COUNTRY: 'AU',
  PHONE_FORMAT : 'e164'
});
