module.exports = Object.freeze({
  ADMIN: 'admin',
  BUYER: 'buyer',
  AGENT: 'agent'
});
