const ROLES = require('./roles');
const VALIDATIONS = require('./validations');
const DEFAULT_VALUES = require('./defaultValues');

const CONSTANTS = Object.freeze({
  ROLES,
  VALIDATIONS,
  DEFAULT_VALUES
});

module.exports = CONSTANTS;
