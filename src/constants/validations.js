const REGEX_PASS = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9^+=$*.[\]{}()?\-"!@#%&/\\,><':;|_~`]{8,32}$/;
// special chars list - https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-settings-policies.html
const REGEX_PASS_OPTIONS = Object.freeze({ name: 'lower, upper, numbers' });
const PASS_CUSTOM_MESSAGE = '"password" fails to match the required lower, upper, numbers pattern';

module.exports = Object.freeze({
  REGEX_PASS,
  REGEX_PASS_OPTIONS,
  PASS_CUSTOM_MESSAGE
});
