const http = require('http');
const config = require('../config');
const app = require('./app');
const utils = require('./utils');
const databaseService = require('./services/database');

const { port } = config;

const {
  logger,
  gracefulShutdown,
  promisifyListen
} = utils;

// create server (replace by https in case of SSL certs)
const server = http.createServer(app);

module.exports = {
  async start() {
    // initialize connection to the database
    await databaseService.connect();

    // add other setup here

    // server listen
    await promisifyListen(server, { port });

    logger.info(`[${new Date().toISOString()}]: Server started at port ${config.port} in "${config.env}" environment`);

    // extend server with graceful shutdown
    gracefulShutdown(server);

    return server;
  }
};
