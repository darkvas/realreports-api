const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');
const morgan = require('morgan');
const {
  logger
} = require('./utils');

const app = express();

// configure CORS & headers
app.use(helmet());
app.use(cors());

// set up body & query parsers
app.use(bodyParser.json({ strict: false, limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

// set logger for requests
app.use(morgan('short', { stream: logger.stream }));

// mount main router
app.use(require('./features/router'));

module.exports = app;
