module.exports = {
  DEFAULT_SUCCESS_MESSAGE: 'Success',
  PAGE_NOT_FOUND         : 'Page Not Found',
  INTERNAL_SERVER_ERROR  : 'Internal Server Error',
  VALIDATION_ERROR       : 'Validation Error',

  CODES: {
    OK                   : 200,
    CREATED              : 201,
    BAD_REQUEST          : 400,
    UNAUTHORIZED         : 401,
    FORBIDDEN            : 403,
    NOT_FOUND            : 404,
    UNPROCESSABLE_ENTITY : 422,
    INTERNAL_SERVER_ERROR: 500
  }
};
