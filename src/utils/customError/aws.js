const http = require('http');
const CONSTANTS = require('../constants');
const CoreError = require('./core');

const HTTP_CODES = CONSTANTS.RESPONSES.CODES;
const HTTP_ERRORS = http.STATUS_CODES;

module.exports = class Aws extends CoreError {
  constructor(awsError) {
    super(awsError.message);

    this.status = awsError.statusCode || HTTP_CODES.INTERNAL_SERVER_ERROR;
    this.name = awsError.name || HTTP_ERRORS[this.status];
    this.message = this.message || HTTP_ERRORS[this.status];
  }
};
