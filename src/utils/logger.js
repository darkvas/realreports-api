const winston = require('winston');

/* eslint-disable no-magic-numbers */
const logger = winston.createLogger({
  levels: {
    trace  : 9,
    input  : 8,
    verbose: 7,
    prompt : 6,
    debug  : 5,
    info   : 4,
    data   : 3,
    help   : 2,
    warn   : 1,
    error  : 0
  },

  format: winston.format.combine(winston.format.timestamp(), winston.format.json()),

  transports: [
    // write to all logs with level `info` and below to `combined.log`
    // write all logs error (and below) to `error.log`.
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' })
  ]
});
/* eslint-enable no-magic-numbers */

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
  write(message) {
    // use the 'info' log level so the output will be picked up by both transports (file and console)
    logger.info(message);
  }
};

logger.add(new winston.transports.Console({
  level           : 'error',
  format          : winston.format.prettyPrint(),
  handleExceptions: false,
  silent          : false
}));

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format          : winston.format.simple(),
    handleExceptions: false,
    silent          : false
  }));
}

module.exports = logger;
