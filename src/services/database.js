const { Pool } = require('pg');
const config = require('../../config');

const {
  host,
  port,
  database,
  user,
  password
} = config.database;

let pool = null;

const createPool = () => {
  return new Pool({
    host,
    port,
    database,
    user,
    password
  });
};

module.exports = {
  connect: () => {
    if (!pool) {
      pool = createPool();
    }
    return pool;
  },

  disconnect: () => {
    if (pool) {
      pool.end();
    }
  },

  query: (text, params) => {
    if (!pool) {
      pool = createPool();
    }
    return pool.query(text, params);
  }
};
