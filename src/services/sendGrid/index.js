const sendGridMail = require('@sendgrid/mail');
const config = require('../../../config/index');
const MailService = require('./mail.service');

sendGridMail.setApiKey(config.sendgrid.apiKey);
const emailService = new MailService(sendGridMail, config.emailFrom);

module.exports = {
  emailService
};
