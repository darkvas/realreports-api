const _ = require('lodash');

class MailService {
  constructor(sendGridMail, emailFrom) {
    this.sendGridMail = sendGridMail;
    this.emailFrom = emailFrom;
  }

  async sendEmail(options) {
    const {
      emailTo,
      subject,
      body
    } = options;

    const msg = {
      to  : emailTo,
      from: this.emailFrom,
      subject,
      html: body
    };

    const result = await this.sendGridMail.send(msg);

    const statusCode = _.get(result, '[0].statusCode');

    if (statusCode === 202) {
      return { MessageId: _.get(result, '[0].headers.x-message-id') };
    }

    return result && result[0];
  }
}

module.exports = MailService;
