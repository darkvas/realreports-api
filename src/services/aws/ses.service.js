const {
  customError: {
    AwsError
  }
} = require('../../utils');

class SesService {
  constructor(ses, emailFrom, AWS) {
    this.ses = ses;
    this.emailFrom = emailFrom;
    this.AWS = AWS;
  }

  createSesEmailParams(options) {
    const {
      emailTo,
      subject,
      body
    } = options;

    return {
      Destination: {
        BccAddresses: [],
        CcAddresses : [],
        ToAddresses : [emailTo]
      },

      Message: {
        Body: {
          Html: {
            Data   : body,
            Charset: 'UTF-8'
          }
        },
        Subject: {
          Data   : subject,
          Charset: 'UTF-8'
        }
      },

      Source          : this.emailFrom,
      ReplyToAddresses: [this.emailFrom],
      ReturnPath      : this.emailFrom
    };
  }

  sendEmail(options) {
    const emailParams = this.createSesEmailParams(options);

    return new Promise((resolve, reject) => {
      // this.AWS.config.credentials.refresh(() => {
      this.ses.sendEmail(emailParams, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
    // });
  }
}

module.exports = SesService;
