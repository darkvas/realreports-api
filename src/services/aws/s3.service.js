const {
  customError: {
    AwsError
  }
} = require('../../utils');

class S3Service {
  constructor(s3, bucket) {
    this.s3 = s3;
    this.bucket = bucket;
  }

  getReadStream({ key, contentType }) {
    const bucket = this.bucket;

    return this.s3.getObject({
      Bucket             : bucket,
      Key                : key,
      ResponseContentType: contentType
    }).createReadStream();
  }

  removeObject({ key }) {
    return new Promise((resolve, reject) => {
      const params = {
        Bucket: this.bucket,
        Key   : key
      };

      this.s3.deleteObject(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }
}

module.exports = S3Service;
