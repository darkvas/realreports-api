const mapAttributeList = require('./mapAttributeList');
const mapTokenData = require('./mapTokenData');
const mapAwsUserData = require('./mapAwsUserData');
const mapSearchObject = require('./mapSearchObject');

module.exports = {
  mapAttributeList,
  mapTokenData,
  mapAwsUserData,
  mapSearchObject
};
