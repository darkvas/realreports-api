const toAWS = {
  email      : 'email',
  firstName  : 'name',
  lastName   : 'family_name',
  phoneNumber: 'phone_number',
  role       : 'custom:role'
};

module.exports = options => Object.keys(toAWS).map(key => ({
  Name : toAWS[key],
  Value: options[key]
}));
