const mapAwsSettings = {
  id         : 'sub',
  email      : 'email',
  firstName  : 'name',
  lastName   : 'family_name',
  phoneNumber: 'phone_number'
};

module.exports = searchObj => {
  if (!searchObj) {
    return null;
  }

  return `${mapAwsSettings[searchObj.field]} ^= "${searchObj.value}"`;
};
