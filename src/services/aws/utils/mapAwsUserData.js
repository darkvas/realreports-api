const mapSettings = {
  'sub'                  : 'id',
  'email'                : 'email',
  'name'                 : 'firstName',
  'family_name'          : 'lastName',
  'phone_number'         : 'phoneNumber',
  'phone_number_verified': 'phoneNumberVerified',
  'custom:role'          : 'role'
};

module.exports = attributes => {
  const user = { // default object
    id                 : null,
    email              : null,
    firstName          : null,
    lastName           : null,
    phoneNumber        : null,
    phoneNumberVerified: null,
    role               : null
  };

  attributes.reduce((userObj, item) => {
    const key = mapSettings[item.Name];
    if (key) {
      userObj[key] = item.Value;
    }
    return userObj;
  }, user);

  return user;
};
