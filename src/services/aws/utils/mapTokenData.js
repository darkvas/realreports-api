const _ = require('lodash');

module.exports = data => {
  const result = {};

  result.accessToken = _.get(data, 'AuthenticationResult.AccessToken', null);
  result.expiresIn = _.get(data, 'AuthenticationResult.ExpiresIn');
  result.refreshToken = _.get(data, 'AuthenticationResult.RefreshToken', null);
  result.idToken = _.get(data, 'AuthenticationResult.IdToken', null);

  return result;
};
