const AWS = require('aws-sdk');
const config = require('../../../config/index');
const AuthCognitoService = require('./authCognito.service');
const S3Service = require('./s3.service');
const SesService = require('./ses.service');

AWS.config.update({
  region         : config.aws.region,
  accessKeyId    : config.aws.accessKeyId,
  secretAccessKey: config.aws.secretAccessKey
});

const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({ apiVersion: '2016-04-18' });
const authCognitoService = new AuthCognitoService(cognitoIdentityServiceProvider);

const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
const imageS3Service = new S3Service(s3, config.aws.s3.bucketImage);
const fileS3Service = new S3Service(s3, config.aws.s3.bucketFile);

const ses = new AWS.SES({ region: config.aws.ses.region });
const emailService = new SesService(ses, config.emailFrom, AWS);

module.exports = {
  authCognitoService,
  imageS3Service,
  fileS3Service,
  emailService
};
