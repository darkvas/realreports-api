const config = require('../../../config');
const {
  customError: {
    AwsError
  }
} = require('../../utils');
const {
  mapAttributeList,
  mapTokenData,
  mapAwsUserData,
  mapSearchObject
} = require('./utils');

class AuthService {
  constructor(cognitoIdentityServiceProvider) {
    this.cognitoIdentityService = cognitoIdentityServiceProvider;
  }

  signIn({ email, password }) {
    return new Promise((resolve, reject) => {
      const params = {
        AuthFlow      : 'USER_PASSWORD_AUTH',
        ClientId      : config.aws.cognito.clientId,
        AuthParameters: {
          USERNAME: email,
          PASSWORD: password
        }
      };

      this.cognitoIdentityService.initiateAuth(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(mapTokenData(data));
      });
    });
  }

  signOut({ accessToken }) {
    return new Promise((resolve, reject) => {
      const params = {
        AccessToken: accessToken
      };

      this.cognitoIdentityService.globalSignOut(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  signUp(options) {
    return new Promise((resolve, reject) => {
      const attributeList = mapAttributeList(options);

      const params = {
        ClientId      : config.aws.cognito.clientId,
        Password      : options.password,
        Username      : options.email,
        UserAttributes: attributeList
      };

      this.cognitoIdentityService.signUp(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  confirmPhone({ email, confirmationCode }) {
    return new Promise((resolve, reject) => {
      const params = {
        ClientId        : config.aws.cognito.clientId,
        Username        : email,
        ConfirmationCode: confirmationCode
      };

      this.cognitoIdentityService.confirmSignUp(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  refreshToken({ refreshToken }) {
    return new Promise((resolve, reject) => {
      const params = {
        AuthFlow      : 'REFRESH_TOKEN_AUTH',
        ClientId      : config.aws.cognito.clientId,
        AuthParameters: {
          REFRESH_TOKEN: refreshToken
        }
      };

      this.cognitoIdentityService.initiateAuth(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(mapTokenData(data));
      });
    });
  }

  updateProfile(options) {
    return new Promise((resolve, reject) => {
      const {
        accessToken,
        firstName,
        lastName
      } = options;

      const params = {
        AccessToken   : accessToken,
        UserAttributes: [
          {
            Name : 'name',
            Value: firstName
          },
          {
            Name : 'family_name',
            Value: lastName
          }
        ]
      };

      this.cognitoIdentityService.updateUserAttributes(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  getUser({ accessToken }) {
    return new Promise((resolve, reject) => {
      const params = {
        AccessToken: accessToken
      };

      this.cognitoIdentityService.getUser(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        return resolve(mapAwsUserData(data.UserAttributes));
      });
    });
  }

  updatePassword(options) {
    return new Promise((resolve, reject) => {
      const {
        accessToken,
        oldPassword,
        newPassword
      } = options;

      const params = {
        AccessToken     : accessToken,
        PreviousPassword: oldPassword,
        ProposedPassword: newPassword
      };

      this.cognitoIdentityService.changePassword(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  forgotPassword({ email }) {
    return new Promise((resolve, reject) => {
      const params = {
        ClientId: config.aws.cognito.clientId,
        Username: email
      };

      this.cognitoIdentityService.forgotPassword(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  confirmForgotPassword(options) {
    return new Promise((resolve, reject) => {
      const {
        email,
        password,
        confirmationCode
      } = options;

      const params = {
        ClientId        : config.aws.cognito.clientId,
        Username        : email,
        Password        : password,
        ConfirmationCode: confirmationCode
      };

      this.cognitoIdentityService.confirmForgotPassword(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  adminGetUser({ id }) {
    return new Promise((resolve, reject) => {
      const params = {
        UserPoolId: config.aws.cognito.userPoolId,
        Username  : id
      };

      this.cognitoIdentityService.adminGetUser(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        const user = mapAwsUserData(data.UserAttributes);
        user.meta = {
          userStatus: data.UserStatus,
          createdAt : data.UserCreateDate,
          updatedAt : data.UserLastModifiedDate
        };
        return resolve(user);
      });
    });
  }

  adminConfirmSignUp({ email }) {
    return new Promise((resolve, reject) => {
      const params = {
        UserPoolId: config.aws.cognito.userPoolId,
        Username  : email
      };

      this.cognitoIdentityService.adminConfirmSignUp(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  getUserList(options) {
    return new Promise((resolve, reject) => {
      const {
        limit,
        paginationToken,
        search
      } = options;

      const params = {
        UserPoolId     : config.aws.cognito.userPoolId,
        AttributesToGet: null, // get all attributes
        Filter         : mapSearchObject(search),
        Limit          : limit,
        PaginationToken: paginationToken
      };

      this.cognitoIdentityService.listUsers(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        const result = {
          users          : [],
          paginationToken: data.PaginationToken || null
        };
        if (data.Users && data.Users.length) {
          result.users = data.Users.map(item => mapAwsUserData(item.Attributes));
        }
        return resolve(result);
      });
    });
  }

  adminUpdateUser(options) {
    return new Promise((resolve, reject) => {
      const {
        id,
        email,
        firstName,
        lastName
      } = options;

      const params = {
        UserPoolId    : config.aws.cognito.userPoolId,
        Username      : id,
        UserAttributes: [
          {
            Name : 'email',
            Value: email
          },
          {
            Name : 'name',
            Value: firstName
          },
          {
            Name : 'family_name',
            Value: lastName
          }
        ]
      };

      this.cognitoIdentityService.adminUpdateUserAttributes(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }

  adminDeleteUser({ id }) {
    return new Promise((resolve, reject) => {
      const params = {
        UserPoolId: config.aws.cognito.userPoolId,
        Username  : id
      };

      this.cognitoIdentityService.adminDeleteUser(params, (err, data) => {
        if (err) {
          return reject(new AwsError(err));
        }
        return resolve(data);
      });
    });
  }
}

module.exports = AuthService;
