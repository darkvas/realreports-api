const nodemailer = require('nodemailer');
const mandrillTransport = require('nodemailer-mandrill-transport');
const mandrillApi = require('mandrill-api/mandrill');
const config = require('../../../config/index');
const MailService = require('./mail.service');

const smtpTransport = nodemailer.createTransport(mandrillTransport({
  auth: {
    apiKey: config.mandrill.apiKey
  }
}));

const attachmentsClient = new mandrillApi.Mandrill(config.mandrill.apiKey);

const emailService = new MailService(smtpTransport, attachmentsClient, config.emailFrom);

module.exports = {
  emailService
};
