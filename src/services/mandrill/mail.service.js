const _ = require('lodash');

// Symbol is used to handle privacy of methods
const sendSimpleEmail = Symbol('sendSimpleEmail');
const sendAttachmentsEmail = Symbol('sendSimpleEmail');

class MailService {
  constructor(smtpTransport, attachmentsClient, emailFrom) {
    this.smtpTransport = smtpTransport;
    this.attachmentsClient = attachmentsClient;
    this.emailFrom = emailFrom;
  }

  async [sendSimpleEmail](options) {
    const {
      emailTo,
      subject,
      body
    } = options;

    const emailOptions = {
      to  : emailTo,
      from: this.emailFrom,
      subject,
      html: body
    };

    const result = await this.smtpTransport.sendMail(emailOptions);

    const acceptedLength = _.get(result, 'accepted.length', 0);

    if (acceptedLength > 0) {
      return { MessageId: _.get(result, 'messageId') };
    }

    return result.rejected;
  }

  [sendAttachmentsEmail](options) {
    const {
      emailTo,
      subject,
      body,
      attachments
    } = options;

    const message = {
      subject,
      attachments,
      html        : body,
      'from_email': this.emailFrom,
      'from_name' : this.emailFrom,

      to: [
        {
          email: emailTo,
          name : emailTo,
          type : 'to'
        }
      ]
    };

    return new Promise((resolve, reject) => {
      this.attachmentsClient.messages.send({ message, async: false }, result => {
        resolve({ MessageId: _.get(result, '[0]._id') });
      }, e => {
        reject(e);
      });
    });
  }

  sendEmail(options) {
    const {
      attachments
    } = options;

    if (attachments && attachments.length) {
      return this[sendAttachmentsEmail](options);
    }
    return this[sendSimpleEmail](options);
  }
}

module.exports = MailService;
