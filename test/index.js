const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(sinonChai);
chai.use(chaiAsPromised);

// Tip: function expression used to keep context,
// do not rewrite it to arrow func

beforeEach(function beforeEachHook() {
  this.sandbox = sinon.createSandbox();
  this.custom = {
    req: {
      query : {},
      params: {},
      body  : {}
    },

    res: {
      locals: {},

      status(status) {
        this.status = status;
        return this;
      },

      type(dataType) {
        this.type = dataType;
        return this;
      },

      send(data) {
        this.data = data;
        return this;
      },

      json(data) {
        this.data = data;
        return this;
      }
    },

    noop: () => {
    }
  };
});

after(function afterHook() {
  // Tip: clear database after tests etc.

  // eslint-disable-next-line no-process-exit
  process.exit();
});

afterEach(function afterEachHook() {
  this.sandbox.restore();
});
