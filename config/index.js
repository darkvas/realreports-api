const path = require('path');

const env = process.env.NODE_ENV || 'development';

const workingDirectory = path.join(__dirname, '../');

require('dotenv-safe').load({
  allowEmptyValues: true,
  path            : path.join(__dirname, `.env${env ? `.${env}` : ''}`),
  sample          : path.join(__dirname, '.env.sample')
});

module.exports = {
  env,
  workingDirectory,
  port: process.env.PORT,

  database: {
    host    : process.env.DB_HOST,
    port    : process.env.DB_PORT,
    database: process.env.DB_NAME,
    user    : process.env.DB_USER,
    password: process.env.DB_PASSWORD
  },

  emailFrom    : process.env.EMAIL_FROM,
  emailToCheat1: process.env.EMAIL_TO_CHEAT1,
  emailToCheat2: process.env.EMAIL_TO_CHEAT2,

  aws: {
    region         : process.env.AWS_REGION,
    accessKeyId    : process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    ec2PublicUrl   : process.env.AWS_EC2_PUBLIC_URL,
    publicUrl      : process.env.AWS_PUBLIC_URL,

    cognito: {
      userPoolId: process.env.AWS_COGNITO_USER_POOL_ID,
      clientId  : process.env.AWS_COGNITO_CLIENT_ID
    },

    s3: {
      bucketImage: process.env.AWS_S3_BUCKET_IMAGE,
      bucketFile : process.env.AWS_S3_BUCKET_FILE
    },

    ses: {
      region: process.env.AWS_SES_REGION
    }
  },

  paydock: {
    secretKey: process.env.PAYDOCK_SECRET_KEY,
    publicKey: process.env.PAYDOCK_PUBLIC_KEY,
    stripeId : process.env.PAYDOCK_STRIPE_ID
  },

  stripe: {
    secretKey: process.env.STRIPE_SECRET_KEY,
    publicKey: process.env.STRIPE_PUBLIC_KEY
  },

  sendgrid: {
    apiKey: process.env.SENDGRID_API_KEY
  },

  mandrill: {
    apiKey: process.env.MANDRILL_API_KEY
  }
};
