const server = require('./src/server');
const {
  logger
} = require('./src/utils');

const setupGracefulShutdown = expressServer => {
  process.on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection at: Promise ', promise, ' reason: ', reason);

    expressServer.shutdown();
  });

  process.on('uncaughtException', ex => {
    logger.error('Unhandled Exception:', ex);

    expressServer.shutdown();
  });

  process.once('SIGTERM', () => {
    logger.info('Received SIGTERM. Graceful shutdown start', new Date().toISOString());

    expressServer.shutdown();
  });

  process.once('SIGINT', () => {
    logger.info('Received SIGINT. Graceful shutdown start', new Date().toISOString());

    expressServer.shutdown();
  });
};

const onStartError = ex => {
  logger.error('Server error: ', ex);

  // eslint-disable-next-line no-process-exit
  process.exit(0);
};

// start the server
server.start()
  .then(setupGracefulShutdown)
  .catch(onStartError);
