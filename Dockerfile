# Official framework image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/node/tags/
FROM node:8.12.0

# Create app directory
WORKDIR /src

# Install app dependencies
COPY package.json .

RUN npm install -q

# Bundle app source
COPY . .

EXPOSE 3000
CMD [ "npm", "start" ]
