#!/bin/bash

# any future command that fails will exit the script
set -e

cd /home/ubuntu/realreports-api

# pull changes
git pull

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
sudo pm2 stop realreports-api
sudo pm2 start index.js --name realreports-api
