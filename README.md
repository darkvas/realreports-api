# Real Reports API

## Installing / Getting started

```
cd realreports/api/
npm install
npm start
```

## Commit convention

Please note to use this template for commit messages:
```
<type>(<scope>): <subject>

<body?>

<footer?>
```
- **\<type\>** - the type of change that you're committing
   - Must be one of the following:
     - **chore**: Changes to the build process or auxiliary tools and libraries such as documentation generation
     - **feat**: A new feature
     - **docs**: Documentation only changes
     - **fix**: A bug fix
     - **refactor**: A code change that neither fixes a bug nor adds a feature
     - **test**: Adding missing tests or correcting existing tests
     - **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
     - **ci**: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
     - **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
     - **perf**: A code change that improves performance

- **\<scope\>** - the scope of this change
- **\<subject\>** - short, imperative tense description of the change

Or use the [Commitizen](https://www.npmjs.com/package/commitizen) cli tools:
```
npm run cz-commit
```

## Docker

To create the docker image and container run the following commands:

Prepare **.env** configs with IP addresses instead of localhost (to have access to database and external api).
Create docker image with given docker file (change EXPOSE PORT or use default):
```
docker build --tag real-report-api .
```

Run the container:
```
docker run -d --name real-report-api -p 3000:3000 real-report-api
```

If something went wrong with container you could check logs:

```
docker cp real-report-api:/src/error.log ./dockerError.log
```

## AWS

Add your ip in security group rules on aws console.
Use ssh to connect to ec2 instance (t2.micro with Ubuntu 18.04).
```
ssh -i realReportsKeys.pem ubuntu@aws-url.com
```

## API Documentation

Good code is its own best documentation.

`Good code is like a good joke — it requires no explanation.`

Anyway, **Postman** is used to check api endpoints. Postman collections are located in **/postman** directory.
Don't forget to prepare postman environment variable **{{url}}** (localhost or ec2 public url).
